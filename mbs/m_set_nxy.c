// N.Kurz, EE, GSI, 3-Sep-2010

#include <stdio.h>
#include <sys/file.h>
#include <sys/types.h>
#include <fcntl.h>
#include <signal.h>
#include <time.h>
#include <timeb.h>
#include <string.h>
#include <sys/resource.h>
#include <smem.h>

#include  "./pexor_gosip.h"
#include "nxyreg.h"
#include "nxyctrl.h"

#define SETUP_FILE "./nxy_explo_set.txt"

//#define MAX_TRG_WIND  0x7ff
#define MAX_TRG_WIND  0xbff

#define GET_BAR0_BASE     0x1234
#define PEXDEV            "/dev/pexor"
#define PCI_BAR0_NAME     "PCI_BAR0_MBS"
#define PCI_BAR0_SIZE     0x800000  // in bytes

static int  l_i, l_j, l_k, l_l;
static s_pexor        sPEXOR;
static long           l_stat;
static int            fd_pex; 
static int            l_bar0_base;
static long volatile *pl_virt_bar0;
static long           l_dat1, l_dat2, l_dat3;

int  f_pex_slave_rd (long, long, long, long*);
int  f_pex_slave_wr (long, long, long,  long);
int  f_pex_slave_init (long, long);
void f_i2c_sleep ();



main ()
{
  	int res;
	struct globalreg greg;
	struct nxyreg r2;
	struct nxyreg *r=NULL;
	memset(&greg,0,sizeof(greg));
	r = load_nxy_file(SETUP_FILE,&greg);
	

  // open PEXOR device 
  if ((fd_pex = open (PEXDEV, O_RDWR)) == -1)
  {
    printf ("ERROR>> could not open %s device \n", PEXDEV);
    exit (0);
  }
  else
  {
    printf ("opened device: %s, fd = %d \n", PEXDEV, fd_pex);
  }

  // get bar0 base:
  l_stat = ioctl (fd_pex, GET_BAR0_BASE, &l_bar0_base);
  if (l_stat == -1 )
  {
      printf ("ERROR>> ioctl GET_BAR0_BASE failed \n");
	}
  else
  {
    printf ("PEXOR bar0 base: 0x%x \n", l_bar0_base);
  } 

  // close pexor device
  l_stat = close (fd_pex);
  if (l_stat == -1 )
  {
    printf ("ERROR>> could not close PEXOR device \n");
	}

  // open shared segment
  smem_remove (PCI_BAR0_NAME);
  if((pl_virt_bar0 = (long *) smem_create (PCI_BAR0_NAME,
            (char*) l_bar0_base, PCI_BAR0_SIZE, SM_READ | SM_WRITE))==NULL)
  {
    printf ("smem_create for PEXOR BAR0 failed");
    exit (-1);
  }

  // initialize gosip on all connected exploders
  PEXOR_GetPointer(0, pl_virt_bar0, &sPEXOR); 

  for (l_i=0; l_i<MAX_SFP; l_i++)          // loop over SFPs
	{
    if (greg.sfp_in_use[l_i][0] > 0)              // sfp l_i used
    {
      for (l_j=0; l_j<MAX_EXP; l_j++)      // find nr. of exploders in this SFP
			{
        if (greg.sfp_in_use[l_i][l_j] == 0) {break;}
      }

      l_stat = f_pex_slave_init (l_i, l_j);  
      if (l_stat == -1)
			{
        printm ("ERROR>> slave address initialization failed \n");
        printm ("exiting...\n"); 
        exit (0); 
      }
    }  
  }

  // setup EXPLODERS and NXYTERS 
  printf ("\nstart setting up ... \n");
  for (l_i=0; l_i<MAX_SFP; l_i++)          // loop over SFPs
	{
    if (greg.sfp_in_use[l_i][0] > 0)              // sfp l_i used
    {
      for (l_j=0; l_j<MAX_EXP; l_j++)      // loop over EXPLODERs
			{
        if (greg.sfp_in_use[l_i][l_j] == 0) {break;}  
        for (l_k=0; l_k<greg.sfp_in_use[l_i][l_j]; l_k++)  // loop over NXYTERs
        {
  		i2c_init_write(r[l_i].sfp,r[l_i].exp);
	   	nxy_write_mask(&r[l_i]); 
	   	nxy_write_bias(&r[l_i]);
        nxy_write_config(&r[l_i]);
        nxy_write_test_delay(&r[l_i]);
		nxy_write_th(&r[l_i]);
        nxy_write_clock_delay(&r[l_i]);
		i2c_deactivate(r[l_i].sfp,r[l_i].exp);
		nxy_adc_setup(&r[l_i]);           
        }

        exploder_reset(l_i,l_j);
		exploder_clock_reset(l_i,l_j);
		global_write(l_i,l_j,greg);     
		exploder_activate_receiver(l_i,l_j,greg);
      }
    }  
  }

  printf ("...setup done \n");
	free(r);

}

/*****************************************************************************/

int f_pex_slave_init (long l_sfp, long l_n_slaves)
{
  int  l_ret;
  long l_comm;

  printf ("initialize SFP chain %d \n", l_sfp);
  l_comm = PEXOR_INI_REQ | (0x1<<16+l_sfp);

  PEXOR_RX_Clear_Ch (&sPEXOR, l_sfp); 
  PEXOR_TX (&sPEXOR, l_comm, 0, l_n_slaves  - 1) ;
  for (l_j=1; l_j<=10; l_j++)
	{
    //printf ("SFP %d: try nr. %d \n", l_sfp, l_j);
    l_dat1 = 0; l_dat2 = 0; l_dat3 = 0;
    l_stat = PEXOR_RX (&sPEXOR, l_sfp, &l_dat1 , &l_dat2, &l_dat3);
		if ( (l_stat != -1) && (l_dat2 > 0) && (l_dat2<=32))
		{
      break;
    }
      yield ();
  }
  l_ret = 0;
  if (l_stat == -1)
  {
    l_ret = -1;
    printf ("ERROR>> initialization of SFP chain %d failed. ", l_sfp);
    printf ("no reply: 0x%x 0x%x 0x%x \n", l_dat1, l_dat2, l_dat3);
    printf ("exiting.. \n"); exit (0);
  }
  else
  {
    if (l_dat2 != 0)
		{ 
      printf ("initialization for SFP chain %d done. \n", l_sfp),
      printf ("No of slaves : %d \n", l_dat2);
    }
    else
		{
      l_ret = -1;
      printf ("ERROR>> initialization of SFP chain %d failed. ", l_sfp);
      printf ("no slaves found. \n"); 
      printf ("exiting.. \n"); exit (0);
    }
  }
  return (l_ret);
}


/*****************************************************************************/

int f_pex_slave_wr (long l_sfp, long l_slave, long l_slave_off, long l_dat)
{
  int  l_ret;
  long l_comm;
  long l_addr;

  l_comm = PEXOR_PT_AD_W_REQ | (0x1<<16+l_sfp);
  l_addr = l_slave_off + (l_slave << 24);
  PEXOR_RX_Clear_Ch (&sPEXOR, l_sfp); 
  PEXOR_TX (&sPEXOR, l_comm, l_addr, l_dat);
  l_stat = PEXOR_RX (&sPEXOR, l_sfp, &l_dat1 , &l_dat2, &l_dat3); 

  l_ret = 0;   
  if (l_stat == -1)
  {
    l_ret = -1;
    #ifdef DEBUG
    printm ("ERROR>> writing to SFP: %d, slave id: %d, addr 0x%d \n",
						                                    l_sfp, l_slave, l_slave_off);
    printm ("  no reply: 0x%x 0x%x 0x%x \n", l_dat1, l_dat2, l_dat3);
    #endif // DEBUG
  }
  else
  {
    // printm ("Reply to PEXOR from SFP: 0x%x ", l_sfp);
    if( (l_dat1 & 0xfff) == PEXOR_PT_AD_W_REP)
    {
      //printm ("SFP: %d, slave id: %d addr: 0x%x  \n",
      //                l_sfp, (l_dat2 & 0xf0000) >> 24, l_dat2 & 0xfffff);
      if ( (l_dat1 & 0x4000) != 0)
      {
        l_ret = -1;
        #ifdef DEBUG
	      printm ("ERROR>> packet structure: command reply 0x%x \n", l_dat1);
        #endif // DEBUG
      }
    }
    else
    {
      l_ret = -1;
      #ifdef DEBUG
      printm ("ERROR>> writing to empty slave or wrong address: \n");
      printm ("  SFP: %d, slave id: %d, 0x%x addr: 0x%x,  command reply:  0x%x \n",
           l_sfp, l_slave, (l_addr & 0xf00000) >> 24 , l_addr & 0xfffff, l_dat1);
      #endif // DEBUG
    }
  }
  //usleep (1);             // Nxyter I2C needs 500us
  f_i2c_sleep ();
  return (l_ret);
}

/*****************************************************************************/

int f_pex_slave_rd (long l_sfp, long l_slave, long l_slave_off, long *l_dat)
{
  int  l_ret;
  long l_comm;
  long l_addr;

  l_comm = PEXOR_PT_AD_R_REQ | (0x1<<16+l_sfp);
  l_addr = l_slave_off + (l_slave << 24);
  PEXOR_RX_Clear_Ch (&sPEXOR, l_sfp); 
  PEXOR_TX (&sPEXOR, l_comm, l_addr, 0);
  l_stat = PEXOR_RX (&sPEXOR, l_sfp, &l_dat1 , &l_dat2, l_dat); 
	//printm ("f_pex_slave_rd, l_dat: 0x%x, *l_dat: 0x%x \n", l_dat, *l_dat);

  l_ret = 0;
  if (l_stat == -1)
  {
    l_ret = -1;
    #ifdef DEBUG
    printm ("ERROR>> reading from SFP: %d, slave id: %d, addr 0x%d \n",
						                      l_sfp, l_slave, l_slave_off);
    printm ("  no reply: 0x%x 0x%x 0x%x \n", l_dat1, l_dat2, *l_dat);
    #endif // DEBUG
  }
  else
  {
    // printm ("Reply to PEXOR from SFP: 0x%x ", l_sfp);
    if( (l_dat1 & 0xfff) == PEXOR_PT_AD_R_REP)
    {
      //printm ("SFP: %d, slave id: %d addr: 0x%x  \n",
      //     l_sfp, (l_dat2 & 0xf00000) >> 24, l_dat2 & 0xfffff);
      if ( (l_dat1 & 0x4000) != 0)
      {
        l_ret = -1;
        #ifdef DEBUG
	      printm ("ERROR>> packet structure: command reply 0x%x \n", l_dat1);
        #endif //DEBUG
      }
    }
    else
    {
      l_ret = -1;
      #ifdef DEBUG 
      printm ("ERROR>> Reading from empty slave or wrong address: \n");
      printm ("  SFP: %d, slave id: %d, 0x%x addr: 0x%x,  command reply:  0x%x \n",
              l_sfp, l_slave, (l_addr & 0xf0000) >> 24 , l_addr & 0xfffff, l_dat1);
      #endif // DEBUG
    }
  }
  //usleep (1);             // Nxyter I2C needs 500us
  f_i2c_sleep ();
  return (l_ret);
}

/*****************************************************************************/

void f_i2c_sleep ()
{
  #define N_LOOP 300000

  int l_ii;
  int volatile l_depp=0; 
 
  for (l_ii=0; l_ii<N_LOOP; l_ii++)
	{
    l_depp++;
  }
}

/*****************************************************************************/
