/**
 * @file
 * A.Prochazka
 * 13.7.2013
 * here are the functions used to calibrate  nxyters from MBS, or read/manipulate settings
 * this is intented to used inside MBS
 * 
 */ 
 
#ifndef NXYJOBS_H
#define NXYJOBS_H

#include <stdlib.h>
#include "nxyreg.h"
#define MAXSTAT 1000



struct nxyreg *_r0;
struct globalreg _greg0;
struct nxyreg *_r;
struct globalreg _greg;

char *nxy_socket_name;

struct nxyhits{
	int num[128];
	int hits[128][MAXSTAT];
};

struct nxyres1{
	int data[256][128];
	int mean[256];
};

struct _nxydata{
	struct nxyhits *phits;
	int nxy_number;
};


typedef enum {NXYJOB_VBIASS_SCAN=1,NXYJOB_VTH_SCAN,NXYJOB_LOCALTH_SCAN,NXYJOB_BASELINES_READ} nxyjobs_t;

struct _nxyjobs{
	nxyjobs_t q[10];
	int index;
	};

int (*nxyjob_function_init)();
int (*nxyjob_function_main)();
int (*nxyjob_function_done)();

int nxydata_event_counter; //count event in saved data
int nxydata_job_stage; //job flow control number
int nxydata_ready;
int nxydata_status;

unsigned int *nxydata_pdata; //pointer to data stream created by MBS 



struct nxyres1 *nxydata_results;
struct _nxydata nxydata;
struct _nxyjobs nxyjobs;

double nxyhits_median(struct nxyhits *n, int ch);
double nxyhits_nhitmean(struct nxyhits *n);
double baselines_mean(int *baseline);

int nxyjob_vbiass_init();
int nxyjob_vbiass_main();
int nxyjob_vbiass_done();

int nxyjob_vth_init();
int nxyjob_vth_main();
int nxyjob_vth_done();

int nxyjob_lth_init();
int nxyjob_lth_main();
int nxyjob_lth_done();

int nxyjob_baselines_init();
int nxyjob_baselines_main();
int nxyjob_baselines_done();

int nxyjob_set();
int nxyjob_process(long *);
int nxyjob_add(nxyjobs_t j);
int nxydata_init(char *_socket_name);
int nxydata_clean();
void nxydata_reset();
#endif
