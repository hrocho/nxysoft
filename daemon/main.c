/**
 * @file
 * A. Prochazka
 * 9.7.2013
 * 
 * This is daemon to control nxyters
 * It is intended to run on a linux computer with SFP access to nxyters
 * headers required: nxyreg.h nxyctrl.h nxysync.h
 */ 
#include <stdio.h>
#include "s_veshe.h"
#include "stdarg.h"
#include <sys/file.h>
#include <sys/types.h>
#include <fcntl.h>
#include <signal.h>
#include <time.h>
#include <string.h>
#include <sys/resource.h>
#include <sys/mman.h>
#include "smem_mbs.h"
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include "./pexor_gosip.h"

#include "sys/socket.h"
#include "sys/un.h"
#include "netinet/in.h"
#include "netdb.h"
#include "arpa/inet.h"

#include <unistd.h>

#include "nxyreg.h"
#include "nxyctrl.h"
#include "nxysync.h"


#define DEBUG 1
#define SETUP_FILE "./nxy_explo_set.txt"
#define GET_BAR0_BASE     0x1234
#define PEXDEV            "/dev/pexor"
#define PCI_BAR0_NAME     "PCI_BAR0_MBS"
#define PCI_BAR0_SIZE     0x800000

static int j;
static s_pexor        sPEXOR;
static long           l_stat;
static int            fd_pex;
//static int            l_bar0_base;
static long volatile *pl_virt_bar0;
static long           l_dat1, l_dat2, l_dat3;

int prot, flags;

// helping functions
int  f_pex_slave_rd (long, long, long, long*);
int  f_pex_slave_wr (long, long, long,  long);
int  f_pex_slave_init (long, long);
void f_i2c_sleep ();
void terminate(int sig);


/// main function
int main(int argc, char **argv)
{
struct globalreg greg0;
struct nxyreg *r0=NULL;
char *filename=SETUP_FILE;
char *buf=NULL,*save=NULL;
int socket_fd;
int status, length;
struct addrinfo hints, *serverinfo,*p;
int yes = 1,i=0,background=0, dosetup=1, doquit=0, dolog=0;
FILE *fout;
pid_t pid, sid;

struct timespec ts;
int waittime = 20;
#ifdef DEBUG
time_t curtime;
#endif

fout = stdout;

/// ctrl-c interuption
struct sigaction saction;
saction.sa_handler = terminate;
sigemptyset(&saction.sa_mask);
saction.sa_flags = 0;
sigaction(SIGINT,&saction,NULL);


nxy_clear_ipc();
status = nxy_init_ipc();
if(status !=1){
	perror("nxy ipc init");
	exit(1);
	}
memset(&greg0,0,sizeof(greg0));

if(argc>1){
	for(i=1;i<argc;i++)
	{
		if(strcmp(argv[i],"-c")==0){
			if((i+1)>=argc){printf("wrong arguments\n");exit(1);}
			filename = argv[i+1];
			i++;
			}
			
		if(strcmp(argv[i],"-b")==0){
			background = 1;
			}

        if(strcmp(argv[i],"-s")==0){
			dosetup = 0;
            }
            
        if(strcmp(argv[i],"-l")==0){
			dolog = 1;
            }
            
        if(strcmp(argv[i],"-q")==0){
			doquit = 1;
            }
		
		}    
    i=0;
}

///kill the existing daemon
if(doquit){
	connect_daemon("localhost");
	buf = pack((int)'Q'<<24,&length,NULL);
	if(buf==NULL){
		printf("error, exiting...\n");
		}
	status = sendall(nxyd_fd,buf,&length);
	if(status!=1){
		printf("Could not send kill signal to the daemon, exiting ...\n");
		}
	free(buf);
    close(nxyd_fd);
    return 1;
	}

// connecting to network socket
memset(&hints,0,sizeof(hints));
hints.ai_family = AF_INET;
hints.ai_socktype = SOCK_STREAM;
hints.ai_flags = AI_PASSIVE;
status = getaddrinfo(NULL,"3490",&hints, &serverinfo);
if(status!=0){exit(1);}
for(p=serverinfo;p!=NULL;p=p->ai_next){
    socket_fd = socket(p->ai_family,p->ai_socktype,p->ai_protocol);
    if(socket_fd<0){fprintf(fout,"could not open socket\n");continue;}
    if( setsockopt(socket_fd,SOL_SOCKET,SO_REUSEADDR, &yes, sizeof(int)) == -1 ){
		exit(1);
		}
    status = bind(socket_fd,p->ai_addr,p->ai_addrlen);
    if(status<0){fprintf(fout,"could not bind to socket, nxy daemon already running ?\n");exit(1);}
    break;
}
if(p==NULL){close(socket_fd);exit(1);}
freeaddrinfo(serverinfo);
status = listen(socket_fd,1);
if(status!=0){fprintf(fout,"could not listen to socket\n");exit(1);}


/// loading settings from asci file
fprintf(fout,"loading %s\n",filename);
r0 = load_nxy_file(filename,&greg0);
if(r0==NULL){fprintf(fout,"could not load config file\n");exit(0);}
//free(r0);
print_global(greg0);

for(i=0;i<greg0.nxy_number;i++){
	struct nxyreg rr;
	nxy_id(i,&rr.sfp,&rr.exp,&rr.nxy,greg0);
	printf("i = %d, %d %d %d\n",i,rr.sfp,rr.exp,rr.nxy);
}

fprintf(fout,"listening ...\n");

///forking
if(background == 1){
	printf("going to background ...\n");
	pid = fork();
	if(pid!=0){
		exit(1);
		}
	umask(0);
	sid = setsid();
	if(sid<0){fprintf(fout,"error while forking, exiting ...\n");exit(1);}
	close(STDIN_FILENO);
	close(STDOUT_FILENO);
	close(STDERR_FILENO);
	fout = fopen("nxyd.log","w");
	}

///init pexor
if ((fd_pex = open (PEXDEV, O_RDWR)) == -1){
	fprintf (fout,"ERROR>> could not open %s device \n", PEXDEV);
	close (fd_pex);          
	exit (0);
	}

prot = PROT_WRITE | PROT_READ;
flags = MAP_SHARED | MAP_LOCKED;
//status = ioctl (fd_pex, GET_BAR0_BASE, &l_bar0_base); //lynx
pl_virt_bar0 = (long*) mmap(NULL,PCI_BAR0_SIZE,prot,flags,fd_pex,0);
if(pl_virt_bar0 == MAP_FAILED){
	fprintf (fout,"ERROR>> ioctl GET_BAR0_BASE failed (%x)\n",pl_virt_bar0);exit(1);
	}

status = close (fd_pex);
if (status == -1 ){
	fprintf (fout,"ERROR>> could not close PEXOR device \n");
}

PEXOR_GetPointer(0, pl_virt_bar0, &sPEXOR);
		
// initializing SFPs
for (i=0; i<MAX_SFP; i++){
	if (greg0.sfp_in_use[i][0] > 0){
		l_stat = f_pex_slave_init (i, get_explo_number(greg0,i));
		if (l_stat == -1){
			fprintf (fout,"ERROR>> slave address initialization failed, exiting ... \n");
			//free(r);exit (0);
			}
		}
	}

/// initializing settings before running the daemon
if(dosetup){
	fprintf(fout,"setting up nxyters ...\n");
	for(i=0;i<greg0.nxy_number;i++){
		status = nxy_write(&r0[i]);
		if(status<1){fprintf(fout,"writing nxy error!\n");exit(1);}
			status = nxy_adc_setup(&r0[i]);
			if(status<1){fprintf(fout,"writing nxy error!\n");exit(1);}
            status = exploder_reset(r0[i].sfp,r0[i].exp);
            if(status<1){fprintf(fout,"writing exp error!\n");exit(1);}

            status = exploder_clock_reset(r0[i].sfp,r0[i].exp);
            if(status<1){fprintf(fout,"writing exp error!\n");exit(1);}

            status = global_write(r0[i].sfp,r0[i].exp,greg0);
            if(status<1){fprintf(fout,"writing exp error!\n");exit(1);}

            status = exploder_activate_receiver(r0[i].sfp,r0[i].exp,greg0);
			if(status<1){fprintf(fout,"writing exp error!\n");exit(1);}
		}
			
		fprintf(fout,"initialized nxyters to: \n");
		for(i=0;i<greg0.nxy_number;i++){
			status = nxy_read(&r0[i]);
			if(status<1){fprintf(fout,"reading back nxy error!\n");exit(1);}
			status = global_read(r0[i].sfp,r0[i].exp,&greg0);
			if(status<1){fprintf(fout,"reading back global error!\n");exit(1);}						
			print_nxy_reg(r0[i]);
		}
		fprintf(fout,"\nglobal settings:\n");
		print_global(greg0);
	}
	
/// main loop
while(1)
{
        struct nxyreg *r=NULL;
        struct globalreg greg;
        struct nxyreg *r1=NULL;
        struct globalreg greg1;
        struct sockaddr_storage client;
		socklen_t client_len=sizeof(client);
		int client_socket_fd;
        int com;
        int data_write=0,data_read=0,data_read_forced = 0,data_reset=0,data_test=0,quit=0,end_session=0,mutlock=0,mutunlock=0;
		int data_write_confirm=0;
        int aok = 1,  bye = 0,iter = 0;

        greg = greg0;
	#ifdef DEBUG
        fprintf(fout,"-------------- accepting new connection -----------------\n");
	#endif
        client_socket_fd = accept(socket_fd,(struct sockaddr *)&client, &client_len);
        if(client_socket_fd<0){fprintf(fout,"could not accept connection\n");continue;}
	#ifdef DEBUG
	curtime = time(NULL);
	if(curtime>-1){
		fprintf(fout,"Accepted: %s\n",ctime(&curtime));
		}
	#endif

        while(!bye)
        {
            iter++;
            fprintf(fout,"iteration = %d\n",iter);
            data_read = 0; data_write=0;data_reset=0; data_test=0;
            buf = unpackdata(client_socket_fd,&length,&com);
            if(buf==NULL){fprintf(fout,"data corrupted, closing connection... \n");break;}

            switch(((com&0xff000000)>>24))
            {
                case 'R': data_read=1;data_read_forced=1;break;
				case 'r': data_read=1;break;
				case 'F': data_read=1;data_read_forced=1;break;
                case 'w': data_write=1;break;
				case 'W': data_write=1;data_write_confirm=1;break;
                case 'T': data_test = 1;data_write=1;break;
                case 'Q': quit=1;break;
				case 'E': end_session = 1;break;
				//case 'L': mutlock = 1;break;
				//case 'U': mutunlock=1;break;
                default: data_write=0;data_read=0;break;
            }

            switch(((com&0xff0000)>>16))
            {
                case 255: data_reset=1;break;
                default: data_reset=0;break;
            }


            switch(((com&0xff00)>>8))
            {
                case 255: bye=0;break;
                default: bye=1;break;
            }

            if(quit==1){bye = 1;break;} //quit the daemon
            if(end_session==1){bye=1;break;}
/*
	    	if(mutlock ==1){
				yes = nxy_mutex->__align;
			if(yes > 0){
				sem_wait(nxy_mutex);
				fprintf(fout,"nxy locked by request ...\n");
			}
		}
            if(mutunlock ==1){				
				yes = nxy_mutex->__align;
				fprintf(fout,"lock value = %d \n",yes);		
			if(yes<1)sem_post(nxy_mutex);
                fprintf(fout,"nxy unlocked by request ...\n");
                }
*/


            //if(data_test==1)data_reset=1;
            if(data_reset==1)com = com|255; // if reset is required update all registers

	#ifdef DEBUG
	    fprintf(fout,"*** command received: %c %x %x %d ***\n",(com&0xff000000)>>24,(com&0xff0000)>>16,(com&0xff00)>>8,(com&0xff));
	    fprintf(fout,"flags: ");
	    if(data_write)fprintf(fout,"data_write ");
            if(data_write_confirm)fprintf(fout,"data_write_confirm ");

	    if(data_read)fprintf(fout,"data_read ");
    	    if(data_read_forced)fprintf(fout,"data_read_forced ");
            if(data_reset)fprintf(fout,"data_reset ");
            if(data_test)fprintf(fout,"data_test ");
            if(bye)fprintf(fout,"bye ");
	    fprintf(fout,"\n");
	#endif
            if(length>100)
                r = str2nxydata(buf,&greg);
            if(r==NULL){ ///!!! needs to be done better way
                r = (struct nxyreg *)malloc(greg0.nxy_number*sizeof(struct nxyreg));
                memcpy(r,r0,greg0.nxy_number*sizeof(struct nxyreg));
            }
            free(buf);
            //if(r==NULL)continue;
		
	    if(data_write||data_read){
			#ifdef DEBUG
	    		fprintf(fout,"waiting to for lock ... \n");
			#endif
			
			clock_gettime(CLOCK_REALTIME,&ts);
			ts.tv_sec+=waittime;
			status = sem_timedwait(nxy_mutex,&ts);
			if(status!=0){
				data_write=0;data_read=0;bye=1;
				status = send(client_socket_fd,"TIM",3,0);
				}
			//nxy_lock();
	    }

		if(data_write){ ///writing nxy registers
                if(memcmp((char *)&greg + (4*sizeof(int32_t)),(char *)&greg0+(4*sizeof(int32_t)),sizeof(struct globalreg)-(6*sizeof(int32_t)))!=0){ //check if global config is the same
                    bye=1;
                    fprintf(fout,"wrong global registers, closing connection \n");
					print_global(greg);
					print_global(greg0);
                    status = send(client_socket_fd,"EGR",3,0);
					nxy_unlock();
                    break;
                }
		#ifdef DEBUG
		fprintf(fout,"Writing nxy registers...\n");
		#endif

			if(data_write_confirm){
				r1=r;
				greg1=greg;
			}		

			for(i=0;i<greg.nxy_number;i++){ //loop over nxyters
                    if(data_reset==0)
                        status = i2c_init(r[i].sfp,r[i].exp);
                       else
                        status = i2c_init_write(r[i].sfp,r[i].exp);

                    if(status<1){fprintf(fout,"writing init nxy error!\n");aok=0;break;}

                    if(com&1){
                        status = nxy_write_mask(&r[i]);
                        if(status<1){fprintf(fout,"writing mask nxy error!\n");aok=0;}
                    }

                    if(com&2){
                        status = nxy_write_bias(&r[i]);
                        if(status<1){fprintf(fout,"writing bias nxy error!\n");aok=0;}
                    }

                    if(com&4){
                        status = nxy_write_config(&r[i]);
                        if(status<1){fprintf(fout,"writing config nxy error!\n");aok=0;}
                    }

                    if(com&8){
                        status = nxy_write_test_delay(&r[i]);
                        if(status<1){fprintf(fout,"writing test delay nxy error!\n");aok=0;}
                    }

                    if(com&16){
                        status = nxy_write_th(&r[i]);
                        if(status<1){fprintf(fout,"writing th nxy error!\n");aok=0;}
                    }

                    if(com&32){
                        status = nxy_write_clock_delay(&r[i]);
                        if(status<1){fprintf(fout,"writing nxy error!\n");aok=0;}
                    }

                    status = i2c_deactivate(r[i].sfp,r[i].exp);
                    
                    if(status<1){fprintf(fout,"writing nxy error!\n");aok=0;}
					
					if(r[i].nxy==0 && data_reset==0){						
						if(com&64){
						status = global_write(r[i].sfp,r[i].exp,greg);
		                if(status<1){fprintf(fout,"writing exp error!\n");aok=0;}
						}
						
						if(com&128){
							if(data_test==1)greg.mode = 1;
							status = exploder_activate_receiver(r[i].sfp,r[i].exp,greg);
							if(status<1){fprintf(fout,"writing exp error!\n");aok=0;}
							}
						}
	
					if(r[i].nxy==0 && data_reset==1){
						status = nxy_adc_setup(&r[i]);
						if(status<1){fprintf(fout,"writing nxy error!\n");aok=0;}
	                        
						status = exploder_reset(r[i].sfp,r[i].exp);
						if(status<1){fprintf(fout,"writing exp error!\n");aok=0;}

						status = exploder_clock_reset(r[i].sfp,r[i].exp);
						if(status<1){fprintf(fout,"writing exp error!\n");aok=0;}

						status = global_write(r[i].sfp,r[i].exp,greg);
						if(status<1){fprintf(fout,"writing exp error!\n");aok=0;}

						if(data_test==1)greg.mode = 1;
						status = exploder_activate_receiver(r[i].sfp,r[i].exp,greg);
						if(status<1){fprintf(fout,"writing exp error!\n");aok=0;}
                    	}
             
						// status = nxy_write(r[i].sfp,r[i].exp,r[i].nxy,&r[i],com&0xff);
						// if(status<1)fprintf(fout,"writing reading error!\n");

           	  } // end of nxyter loop
           	
		#ifdef DEBUG
		fprintf(fout,"Nxyter test mode = %d\n",greg.mode);
		#endif

		if(data_write_confirm){/// read out the setting back and compare with what it is suppose to be
			#ifdef DEBUG
			fprintf(fout,"confirming written setting...");
			#endif
			nxyregcpy(&r1, &r, &greg1, &greg);		
			for(i=0;i<greg0.nxy_number;i++){
				status = nxy_read(&r1[i]);
				if(status<1){fprintf(fout,"nxy reading error!\n");aok=0;}
				status = global_read(r1[i].sfp,r1[i].exp,&greg1);
				if(status<1){fprintf(fout,"global reading error (%d)!\n",status);aok=0;}
               			}
			save = nxydata2str(greg,r,&length);
			memcpy(nxy_shm, save, length);		
			free(save);
			if(memcmp(r,r1,greg0.nxy_number*sizeof(struct nxyreg))!=0)aok=0;
			if(memcmp(&greg1,&greg,sizeof(struct globalreg))!=0)aok=0;
			#ifdef DEBUG
			if(aok==1)fprintf(fout,"confirmed\n");
			else fprintf(fout,"not confirmed!\n");
			#endif
		}
			

            usleep(20000);
            if(aok==1){
                status = send(client_socket_fd,"AOK",3,0);
                if(dolog==1 && greg.mode==0){
					struct tm * timeinfo;
					char *flogname[23];
					timeinfo = localtime (&curtime);
					strftime (flogname,23,"nxy_%y_%m_%e_%R.txt",timeinfo);
					status = save_nxy_file(flogname,greg,r);
					#ifdef DEBUG
					if(status==1){
					    fprintf(fout,"Nxyter settings saved to %s\n",flogname);
					}
					else {
					    fprintf(fout,"failed to save nxyter settings to file\n");
					}
					#endif
					}
				}
            else{
				fprintf(fout,"write error\n");
				status = send(client_socket_fd,"BAD",3,0);
				bye=1; //close the session after error
                }
	} //end of if data_write


	if(data_read){
		if(data_read_forced == 1){
			#ifdef DEBUG
			fprintf(fout,"Retrieving config from nxyters ...\n");
			#endif
			for(i=0;i<greg0.nxy_number;i++){
				status = nxy_read(&r[i]);
				if(status<1){fprintf(fout,"nxy reading error!\n");aok=0;}
				status = global_read(r[i].sfp,r[i].exp,&greg);
				if(status<1){fprintf(fout,"global reading error (%d)!\n",status);aok=0;}
               			}
			save = nxydata2str(greg,r,&length);
			memcpy(nxy_shm, save, length);
			free(save);
			}
		
		save = nxy_shm;
		length = sizeof(struct globalreg) + (greg.nxy_number * sizeof(struct nxyreg));
		buf = pack(com,&length,save);				
		save = NULL;

		if(buf==NULL){free(r);free(buf);fprintf(fout,"error while preparing data \n");close(client_socket_fd);aok=0;continue;}
		if(aok=1)status = sendall(client_socket_fd,buf,&length);
		else send(client_socket_fd,"BAD",3,0);
		if(status<1){fprintf(fout,"could not send data\n");}
		free(buf);
		#ifdef DEBUG
		fprintf(fout,"Nxyter config sent\n");
		#endif
        } //end of data_read
	
	if(data_write||data_read){
		#ifdef DEBUG
		fprintf(fout,"unlocking ... \n");
		#endif
		nxy_unlock();
		}


	if(bye)break;
	free(r);
	}//end of while(!bye)

	free(r);
    // status = send(client_socket_fd,"AOK",3,0);
    //if(status<1){continue;}
	if(quit==1){
		status = send(client_socket_fd,"AOK",3,0);
		close(client_socket_fd);
		break;
        }
	/// sample to read setting from shared memory
	/*
	printf("shm address = %u\n",nxy_shm);	
	r = str2nxydata(nxy_shm,&greg);
	if(r!=NULL){
		print_global(greg);
		print_nxy_reg(r[0]);
	}
	free(r);
	*/
	 
	#ifdef DEBUG
        fprintf(fout,"closing session...\n");
	#endif
        close(client_socket_fd);
}
fprintf(fout,"exiting ... \n");
free(r0);
close(socket_fd);
fclose(fout);
nxy_clear_ipc();
return 0;
}


void terminate(int sig){
	printf("exiting ...\n");
	nxy_clear_ipc();
	exit(1);

}


/*****************************************************************************/
int f_pex_slave_init (long l_sfp, long l_n_slaves)
{
  int  l_ret;
  long l_comm;

  printf ("initialize SFP chain %d \n", l_sfp);
  l_comm = PEXOR_INI_REQ | (0x1<<16+l_sfp);

  PEXOR_RX_Clear_Ch (&sPEXOR, l_sfp);
  PEXOR_TX (&sPEXOR, l_comm, 0, l_n_slaves  - 1) ;
  for (j=1; j<=10; j++)
	{
    //printf ("SFP %d: try nr. %d \n", l_sfp, l_j);
    l_dat1 = 0; l_dat2 = 0; l_dat3 = 0;
    l_stat = PEXOR_RX (&sPEXOR, l_sfp, &l_dat1 , &l_dat2, &l_dat3);
		if ( (l_stat != -1) && (l_dat2 > 0) && (l_dat2<=32))
		{
      break;
    }
      //yield (); //lynx
      sched_yield();
  }
  l_ret = 0;
  if (l_stat == -1)
  {
    l_ret = -1;
    printf ("ERROR>> initialization of SFP chain %d failed. ", l_sfp);
    printf ("no reply: 0x%x 0x%x 0x%x \n", l_dat1, l_dat2, l_dat3);
    printf ("exiting.. \n"); exit (0);
  }
  else
  {
    if (l_dat2 != 0)
		{
      printf ("initialization for SFP chain %d done. \n", l_sfp),
      printf ("No of slaves : %d \n", l_dat2);
    }
    else
		{
      l_ret = -1;
      printf ("ERROR>> initialization of SFP chain %d failed. ", l_sfp);
      printf ("no slaves found. \n");
      printf ("exiting.. \n"); exit (0);
    }
  }
  return (l_ret);
}


/*****************************************************************************/

int f_pex_slave_wr (long l_sfp, long l_slave, long l_slave_off, long l_dat)
{
  int  l_ret;
  long l_comm;
  long l_addr;

  l_comm = PEXOR_PT_AD_W_REQ | (0x1<<16+l_sfp);
  l_addr = l_slave_off + (l_slave << 24);
  PEXOR_RX_Clear_Ch (&sPEXOR, l_sfp);
  PEXOR_TX (&sPEXOR, l_comm, l_addr, l_dat);
  l_stat = PEXOR_RX (&sPEXOR, l_sfp, &l_dat1 , &l_dat2, &l_dat3);

  l_ret = 0;
  if (l_stat == -1)
  {
    l_ret = -1;
    #ifdef DEBUG
    printm ("ERROR>> writing to SFP: %d, slave id: %d, addr 0x%d \n",
						                                    l_sfp, l_slave, l_slave_off);
    printm ("  no reply: 0x%x 0x%x 0x%x \n", l_dat1, l_dat2, l_dat3);
    #endif // DEBUG
  }
  else
  {
    // printm ("Reply to PEXOR from SFP: 0x%x ", l_sfp);
    if( (l_dat1 & 0xfff) == PEXOR_PT_AD_W_REP)
    {
      //printm ("SFP: %d, slave id: %d addr: 0x%x  \n",
      //                l_sfp, (l_dat2 & 0xf0000) >> 24, l_dat2 & 0xfffff);
      if ( (l_dat1 & 0x4000) != 0)
      {
        l_ret = -1;
        #ifdef DEBUG
	      printm ("ERROR>> packet structure: command reply 0x%x \n", l_dat1);
        #endif // DEBUG
      }
    }
    else
    {
      l_ret = -1;
      #ifdef DEBUG
      printm ("ERROR>> writing to empty slave or wrong address: \n");
      printm ("  SFP: %d, slave id: %d, 0x%x addr: 0x%x,  command reply:  0x%x \n",
           l_sfp, l_slave, (l_addr & 0xf00000) >> 24 , l_addr & 0xfffff, l_dat1);
      #endif // DEBUG
    }
  }
  //usleep (1);             // Nxyter I2C needs 500us
  f_i2c_sleep ();
  return (l_ret);
}

/*****************************************************************************/

int f_pex_slave_rd (long l_sfp, long l_slave, long l_slave_off, long *l_dat)
{
  int  l_ret;
  long l_comm;
  long l_addr;

  l_comm = PEXOR_PT_AD_R_REQ | (0x1<<16+l_sfp);
  l_addr = l_slave_off + (l_slave << 24);
  PEXOR_RX_Clear_Ch (&sPEXOR, l_sfp);
  PEXOR_TX (&sPEXOR, l_comm, l_addr, 0);
  l_stat = PEXOR_RX (&sPEXOR, l_sfp, &l_dat1 , &l_dat2, l_dat);
	//printm ("f_pex_slave_rd, l_dat: 0x%x, *l_dat: 0x%x \n", l_dat, *l_dat);

  l_ret = 0;
  if (l_stat == -1)
  {
    l_ret = -1;
    #ifdef DEBUG
    printm ("ERROR>> reading from SFP: %d, slave id: %d, addr 0x%d \n",
						                      l_sfp, l_slave, l_slave_off);
    printm ("  no reply: 0x%x 0x%x 0x%x \n", l_dat1, l_dat2, *l_dat);
    #endif // DEBUG
  }
  else
  {
    // printm ("Reply to PEXOR from SFP: 0x%x ", l_sfp);
    if( (l_dat1 & 0xfff) == PEXOR_PT_AD_R_REP)
    {
      //printm ("SFP: %d, slave id: %d addr: 0x%x  \n",
      //     l_sfp, (l_dat2 & 0xf00000) >> 24, l_dat2 & 0xfffff);
      if ( (l_dat1 & 0x4000) != 0)
      {
        l_ret = -1;
        #ifdef DEBUG
	      printm ("ERROR>> packet structure: command reply 0x%x \n", l_dat1);
        #endif //DEBUG
      }
    }
    else
    {
      l_ret = -1;
      #ifdef DEBUG
      printm ("ERROR>> Reading from empty slave or wrong address: \n");
      printm ("  SFP: %d, slave id: %d, 0x%x addr: 0x%x,  command reply:  0x%x \n",
              l_sfp, l_slave, (l_addr & 0xf0000) >> 24 , l_addr & 0xfffff, l_dat1);
      #endif // DEBUG
    }
  }
  //usleep (1);             // Nxyter I2C needs 500us
  f_i2c_sleep ();
  return (l_ret);
}

/*****************************************************************************/

void f_i2c_sleep ()
{
  #define N_LOOP 300000

  int l_ii;
  int volatile l_depp=0;

  for (l_ii=0; l_ii<N_LOOP; l_ii++)
	{
    l_depp++;
  }
}

/******************************************************************************/
