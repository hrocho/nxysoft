/* 
 * Andrej Prochazka - a.prochazka@gsi.de
 * GEMEX Go4 library
 *
 * to add to Go4:
 * 1. edit config section below
 * 2. and add GemexSystem variable into Go4, note namespace Gemex
 * 3. Add Unpacker at proper place and event subevent type
 * 4. Create and fill histograms using MakeHist() and FillHist functions
 *
 * */
#ifndef GEMEXGO4_H
#define GEMEXGO4_H


/////////////////////// configuration ////////////////////////////////////// 

#define NGEMEXCARDS 3 ///number of gemex cards, just a guess

/// SFP configuration - number of nxyters per SFP and exploder
/// Exploder ID:     0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15
#define SFP0_CONFIG {2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
#define SFP1_CONFIG {2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
#define SFP2_CONFIG {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
#define SFP3_CONFIG {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}


#define GEMEX_HIS 1 // create histograms for Go4
#define GEMEX_PRINT 1 //printing info

/// preallocated memory to store certain number of hits per gemex/trig/ 
#define NUM_HITS_RESERVED 100

//////////////////////////// end of configuration //////////////////////

#include <vector>
#ifdef GEMEX_HIS
#include "TH2.h"
#include "TH1.h"
#include "TGo4EventProcessor.h"
#include <TGo4AnalysisImp.h>
#endif


namespace Gemex{

class GemexHit{
	public:
	int adc;
	int time;
	int ch;

	GemexHit(int _c,int _a, int _t):adc(_a),time(_t),ch(_c){}
	};


class GemexData{
	public:
	GemexData(int _s, int _e);
	GemexData():sfp(-1),exp(-1){GemexData(-1,-1);}
	
	void Clear();
	void Insert(GemexHit _hit);
	void SetSFP(int _s){sfp=_s;}
	int GetSFP(){return sfp;}
	int GetEXP(){return exp;}
	int GetNData(){return data.size();}
	GemexHit* GetData(int i){return &data[i];}

	int sfp;
	int exp;
	std::vector<GemexHit> data;
	int baseline[256];
	#ifdef GEMEX_HIS
	TH2I *hadc;
	TH2I *hadc_raw;
	//TH2I *htime;
	TH1I *hhit;
	#endif
	};
	

const int sfp_config[4][16] = {SFP0_CONFIG,SFP1_CONFIG,SFP2_CONFIG,SFP3_CONFIG};
	
class GemexSystem{
	public:
	int config[4][16];

	GemexSystem();
	~GemexSystem();
	void Print();
	int Unpack(unsigned int* pdata);
	void ClearHits();
	void LoadBaselineFromFile(char *fname);
	GemexData* GetCard(int i){return &cards[i];}
	GemexData* GetCard(int sfp, int exp){return &cards[config[sfp][exp]];}
	
	#ifdef GEMEX_HIS
	void MakeHist(char *dirname);
	void FillHist();
	#endif
	
	private:
	int ncards;
	std::vector<GemexData> cards;
	
	
	};


} //end of namespace Gemex
#endif
