/**
 * @file
 * A.Prochazka
 * 13.7.2013
 * here are the functions used to manipulate with global and nxyter registers
 * including function for communicating with the nxyter daemon
 * 
 */ 
#ifndef NXYREG_H
#define NXYREG_H
#define DEBUG


#define NXYD_PORT "3490"
#define MAX_SFP 4
#define MAX_EXP 16
#define MAX_NXY 2

#include <stdint.h>

/**
 * enumeration type to indicate which register to update 
 * is is user for nxy_write* function from nxyctrl.h
 */
enum nxy_update_t {NXY_MASK=1,NXY_BIAS=2,NXY_CONFIG=4,
					NXY_TEST_DELAY=8,NXY_TH=16,
					NXY_CLOCK_DELAY=32, NXY_GLOBAL = 64, NXY_RECEIVER = 128, NXY_ALL = 255};

/**
 * structure to store registers values for 1 nxyter, additionally to register it contains 
 * variable do identification of the nxyter, ie sfp, exp and nxy number.
 */
struct nxyreg{
  int32_t i2c_addr;
  int32_t reset;
  int32_t mask[16];
  int32_t bias[14];
  int32_t config[2];
  int32_t te_del[2];
  int32_t clk_del[3];
  int32_t thr_te;
  int32_t thr[128];
  int32_t adc_dco_phase;
  int32_t sfp;
  int32_t exp;
  int32_t nxy;
};

/**
 * structure to store global register values.
 * additionally it contains data:
 * - about sfp configurations:- sfp_in_use, 
 * - nxy_number - total number of nxyters and mode - working mode
 * - mode = 0 is normal operation mode,
 * - mode = 1 is test mode
 * 
 */ 
struct globalreg{

	int32_t pre_trg_wind;
	int32_t pos_trg_wind;
	int32_t test_pul_del;
	int32_t test_trg_del;
  	int32_t sfp_in_use[MAX_SFP][MAX_EXP];
  	int32_t nxy_number;
  	int32_t mode; // exploder mode 0 = normal mode, 1 = test mode
};

/** filedescriptor for socket to connect to nxyter daemon **/
extern int nxyd_fd;

/**
 * This function return the nxyter number (index) according to specified sfp, exp and nxy 
 * parameter and supplied global configuration from globalreg structure @see globalreg
 * @return nxy index on success, on fail it returns -1
 */ 
int nxy_map(int sfp, int exp, int nxy, struct globalreg greg);

/**
 * This function return the sfp, exp and nxy number of the nxyter with nxyter index specified by parameter index 
 * and supplied global configuration from globalreg structure @see globalreg
 * parameters *sfp, *exp and *nxy will be updated
 * @return on success return 1, otherwise 0
 */ 
int nxy_id(int index, int *sfp, int *exp, int *nxy, struct globalreg greg);

//int nxyreg_prepare(struct nxyreg *r, struct globalreg greg);

/**
 * Copies the nxyreg structure from r0 to r and globalreg structure from g0 to greg
 * if r need to be allocated *r should poits to NULL and it will be allocated
 * otherwise it is assumed the memory is already allocated with sufficient size
 * @param  **r is the destination nxyreg structure pointer.
 * @param  **r0 is the source from where nxyreg structure
 * @param *g is pointer to the destination globalreg structure
 * @param *g0 is the pointer to the source globareg structure
 * @return pointer to pointer to destination nxyreg structure , on fail NULL is returned
 */ 

void* nxyregcpy(struct nxyreg **r, struct nxyreg **r0, struct globalreg *g, struct globalreg *g0);

/**
 * function return the number of exploder/Gemex card number at specified sfp according
 * to supplied globalreg greg structure, which needs to be properly loaded
 * @param greg globalreg structure
 * @param sfp sfp number
 * @return returns number of exploders/Gemex cards on specified sfp
 */
int get_explo_number(struct globalreg greg, int sfp);

/**
 * Prints the global register values and configuration to stdout
 * @param greg globalreg structure to be printed
 */ 
void print_global(struct globalreg greg);

/**
 * Loads the nxyter and global register and configuration from txt file
 * Globalreg structure *greg will be updated according to the file settings.
 * the function will allocate memory for nxyreg structures and returns pointer to it.
 * The user is responsible to free the memory
 * @param filename name of the txt file
 * @param *greg pointer to globalreg structure
 * @return pointer to nxyreg structure, on NULL on failure
 * @warning user should free the returned pointer to nxyreg structures
 */ 
struct nxyreg* load_nxy_file(char *filename, struct globalreg *greg);



/**
 * Prints the nxyreg values to stdout.
 * Only one nxyter is printed, to print more function has to be looped
 * @param reg nxyreg structure to be printed
 */ 
void print_nxy_reg(struct nxyreg reg);



/** 
 * Saves the global settings and nxyter registers to the txt file
 * @param name name of the file to be saved
 * @param greg globalreg structure to be saved
 * @param *r pointer to nxyreg structures to be saved
 * @return 1 on success, 0 on failure
 */
int save_nxy_file(char *name, struct globalreg greg, struct nxyreg *r);

/** 
 * Saves the global settings and nxyter registers to the file in binary form.
 * @param name name of the file to be saved
 * @param greg globalreg structure to be saved
 * @param *r pointer to nxyreg structures to be saved
 * @return 1 on success, 0 on failure
 */
int save_bin_file(char *name, struct globalreg greg, struct nxyreg *r);


/** 
 * Loads the global settings and nxyter registers from binary file.
 * memory for nxyreg structures will be allocated,
 * the user is responsible to free it.
 * @param name name of the file to be saved
 * @param greg globalreg structure to be saved
 * @return pointer to the nxyreg structures , NULL on failure
 */
struct nxyreg* load_bin_file(char *name, struct globalreg *greg);

///////////////////  Socket helping function //////////////////////////
/** 
 * Sends the data stream *buf of length *len to the network socked s
 * network socket should be already configured
 * len will be updated by length of actually sent data
 * @param s network socket
 * @param *buf data to send
 * @param *len length of the data to send 
 * @return 1 in success, -1 on failure
 */ 
int sendall(int s, char *buf, int *len);

/**
 * Receive the data stream of length len from network socket s to memory *buf
 * network socket should be already configured
 * @param s network socket
 * @param *buf pointer to destination memory 
 * @param len length of the data to be received
 * @return length of the received data
 */ 
int recvall(int s, char *buf, int len);

/**
 * Packs the data stream *data of length *length into header and trailers with command com
 * the function allocate another buffer which already includes header, data and trailer
 * the length variable will be updated with new size including header+data+trailer
 * the new data stream will be allocated and returned, user is responsible to free it.
 * @param com command number to be packed
 * @param *length length of the data
 * @param *data pointer to data memory 
 * @return pointer to allocated packet data, or NULL on failure
 * 
 */ 
char* pack(int com, int *length, char *data);

/**
 * Unpacks the data from network socket fd,
 * The function will check header and trailer and update *len with length of the data stream received
 * The function will allocate the buffer to store received data.
 * The user is responsible to free it.
 * @param fd network socket
 * @param *len pointer to the integer where length will be stored
 * @param *com pointer to the integet where command number will be stored
 * @return pointer to the received data, or NULL on failure
 */ 
char* unpackdata(int fd,int *len,int *com);

/**
 * Creates the data stream from globalreg and nxyreg structures specified in parameters
 * the data stream will be allocated and len variable updated with actuall size.
 * user is responsible to free the memory
 * @param greg globalreg structure to be written
 * @param *r pointer to nxyreg structures to be written
 * @param *len is pointer to length variable
 * @return pointer to the data stream or NULL on failure
 */ 
char* nxydata2str(struct globalreg greg, struct nxyreg *r,int *len);


/**
 * Converts the data stream with binary coded globalreg and nxyreg structures
 * to globalreg and nxyreg structures.
 * memory for nxyreg structures will be allocated. User is responsible to free it.
 * @param buf pointer to data stream to be read
 * @param *greg pointer to globalreg structure, which will be updated
 * @return pointer to nxyreg structures or NULL on failure
 */ 
struct nxyreg* str2nxydata(char* buf, struct globalreg *greg);

/**
 * Get the filedescriptor of network socket for connection to nxyter daemon
 * nxy_fd global variable will be updated by filedescriptor nad will be returned as well.
 * @param sname name of the computer where nxyter daemon is running
 * @return nxy_fd file descriptor for connection to sname mashine or -1 on failure
 */ 
int connect_daemon(char *sname);


/**
 * Close the network connection with the nxyter daemon running on socket nxy_fd
 * @return 1 on success, -1 on failure
 */
int close_daemon();

/**
 * Reads the current nxyter settings from nxydaemon on network socket nxy_fd.
 * nxy_fd needs to be already configured by callin connect_daemon() function.
 * Retreived configuration will be written to *r and greg structures.
 * @param **r pointer to pointer to nxyreg structures \
 * @param *greg pointer to globalreg structure
 * @return 1 on success, -1 on failure
 */
int read_daemon(struct nxyreg **r, struct globalreg *greg);

/**
 * Writes the configurations specified by r and greg to nxyter daemon running on nxy_fd.
 * nxy_fd needs to be already configured by callin connect_daemon() function.
 * The register sets to be updated can be choosen by mode parameter.
 * The mode parameter should correspond to nxy_update enum.
 * @param *r pointer to nxyreg structures \
 * @param greg globalreg structure
 * @param mode update mode, should be from nxy_update_t enum.
 * @return 1 on success, -2 on sending failure, -1 on updating error
 */
int update_daemon(struct nxyreg *r, struct globalreg greg,int mode);


/**
 * Very similar to update_daemon, but resets the nxyters as well.
 * By default all registers will be updated.
 * The testmode parameters can be set to choose working mode.
 * - testmode = 0 is normal mode
 * - testmode = 1 is test mode
 * @param *rr pointer to nxyreg structures \
 * @param gg globalreg structure
 * @param testmode testmode selection
 * @return 1 on success, -2 on sending failure, -1 on updating error
 */
int reset_daemon(struct nxyreg *rr, struct globalreg gg, int testmode);
#endif
