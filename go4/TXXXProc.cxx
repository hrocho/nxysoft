// N.Kurz, EE, GSI, 15-Jan-2010

//-------------------------------------------------------------
//        Go4 Release Package v3.03-05 (build 30305)
//                      05-June-2008
//---------------------------------------------------------------
//   The GSI Online Offline Object Oriented (Go4) Project
//   Experiment Data Processing at EE department, GSI
//---------------------------------------------------------------
//
//Copyright (C) 2000- Gesellschaft f. Schwerionenforschung, GSI
//                    Planckstr. 1, 64291 Darmstadt, Germany
//Contact:            http://go4.gsi.de
//----------------------------------------------------------------
//This software can be used under the license agreements as stated
//in Go4License.txt file which is part of the distribution.
//----------------------------------------------------------------
#include "TXXXProc.h"

#include "Riostream.h"

#include "TMath.h"
#include "TH1.h"
#include "TH2.h"
#include "TCutG.h"
#include "snprintf.h"

#include "TGo4MbsEvent.h"
#include "TGo4WinCond.h"
#include "TGo4PolyCond.h"
#include "TGo4CondArray.h"
#include "TGo4Picture.h"
#include "TXXXParam.h"
#include "TGo4Fitter.h"

//#include "nxyreg.h"
#include "nxyreg.c"

#define CHARGE 0 

#define CLEAR_PAT    999999

static  UInt_t         l_trigger_counter;

static  UInt_t         l_dat_fir;
static  UInt_t         l_dat_sec;

static UInt_t          l_evt_ct=0;

struct nxyreg *_r=NULL;
struct globalreg _g;
//***********************************************************
TXXXProc::TXXXProc() : TGo4EventProcessor("Proc")
{
  cout << "**** TXXXProc: Create instance " << endl;
}
//***********************************************************
TXXXProc::~TXXXProc()
{
  cout << "**** TXXXProc: Delete instance " << endl;
}
//***********************************************************
// this one is used in standard factory
TXXXProc::TXXXProc(const char* name) : TGo4EventProcessor(name)
{
  cout << "**** TXXXProc: Create instance " << name << endl;

  // create histograms
	gs.MakeHist("GEMEX");
 
  // create comparing histograms over different Nxyter.

}
//


//-----------------------------------------------------------
// event function
Bool_t TXXXProc::BuildEvent(TGo4EventElement* target)
{  // called by framework. We dont fill any output event here at all

  UInt_t         l_check;
  UInt_t        *pl_se_dat;
  UInt_t        *pl_tmp;
  UInt_t         l_padd;  

  TGo4MbsSubEvent* psubevt;

  fInput = (TGo4MbsEvent* ) GetInputEvent();
  if(fInput == 0)
  {
    cout << "AnlProc: no input event !"<< endl;
    return kFALSE;
  }

  if(fInput->GetTrigger() > 11)
  {
    cout << "**** TXXXProc: Skip trigger event"<<endl;
    return kFALSE;
  }



  // first we fill the arrays fCrate1,2 with data from MBS source
  // we have up to two subevents, crate 1 and 2
  // Note that one has to loop over all subevents and select them by
  // crate number:   psubevt->GetSubcrate(),
  // procid:         psubevt->GetProcid(),
  // and/or control: psubevt->GetControl()
  // here we use only crate number

  //printf ("check next event \n"); fflush(stdout); sleep (1);

  l_evt_ct++;
  if(!(l_evt_ct%1000))
	printf("%d\n",l_evt_ct);

  fInput->ResetIterator();

 if(fInput->GetTrigger() > 24)
  {
//    cout << "**** TXXXProc: Skip trigger event"<<endl;
    return kFALSE;
  }
  
  while((psubevt = fInput->NextSubEvent())!=0)
    {
    //printf("procid = %d\n",psubevt->GetProcid());
    if((psubevt->GetProcid())==1){
	pl_se_dat = (UInt_t *)psubevt->GetDataField();
	pl_tmp = pl_se_dat;
	Int_t lenMax = (psubevt->GetDlen()-2)/2;
	if (lenMax<2){
                cout << "Bad User subevent data" << endl;
                return 0;
        	}

	if(fInput->GetTrigger()!=3 && fInput->GetTrigger()<12  ){
		gs.Unpack(pl_se_dat);
		gs.FillHist();
	}
	
	if(fInput->GetTrigger()==3){

		if((*pl_se_dat)&0xffff0000!=0xcfcf0000)continue;
		int nxynum = (*pl_se_dat)&0xffff;
		pl_se_dat++;
		
		if(*pl_se_dat==0xcfcfcccc){ //reading nxy registers
			lenMax = nxynum*sizeof(struct nxyreg);
			lenMax+= sizeof(struct globalreg);
			pl_se_dat++;
			
			_r = str2nxydata((char*)pl_se_dat,&_g);
			/*
			print_global(_g);
			for(int i=0;i<_g.nxy_number;i++){
				print_nxy_reg(_r[i]);
				}
			*/
			pl_se_dat+=lenMax/sizeof(pl_se_dat);			
			}
		
		if(*pl_se_dat==0xcfcfbbbb){ //reading nxy baselines		
			pl_se_dat++;
		    for(int i=0;i<nxynum;i++){
		    	for(int j=0;j<128;j++){
		    		gs.GetCard(_r[i].sfp,_r[i].exp)->baseline[j+(128*_r[i].nxy)] = *pl_se_dat;
		    		pl_se_dat++;
		    		//printf("SFP = %d, EXP = %d, Ch = %d, baseline = %d\n",gs.GetCard(_r[i].sfp,_r[i].exp)->sfp,gs.GetCard(_r[i].sfp,_r[i].exp)->exp,j+(128*_r[i].nxy),gs.GetCard(_r[i].sfp,_r[i].exp)->baseline[j+(128*_r[i].nxy)] );
				}
		    	}	
			}
		if(*pl_se_dat!=0xeecfcfee){
			printf("wrong config traile - %x !!! \n",*pl_se_dat);
			}
		
	}
        continue;
                }

	}//end of subevent loop



bad_event:


  return kTRUE;
}


void dump(unsigned int *pdata,int num){
    printf("------- data dump ------\n");
    for(int i=0;i<num;i++){
	printf("%x ",*(pdata+i));
    }
    printf("-----------------------\n");

}

//----------------------------END OF GO4 SOURCE FILE ---------------------
