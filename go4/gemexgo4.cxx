/*
 * Andrej Prochazka
 * august 2014
 */
#include "gemexgo4.h"
#include <cstring>
#include <stdio.h>
#include <fstream>
namespace Gemex{

//// GemexData ////m
GemexData::GemexData(int _s, int _e){
	data.reserve(NUM_HITS_RESERVED);
	sfp=_s;
	exp=_e;
	for(int i=0;i<256;i++)
		{
			baseline[i]=2500;
			}
	}

void GemexData::Insert(GemexHit _hit){
	data.push_back(_hit);
	}

void GemexData::Clear(){
	data.clear();
	}

//// GemexSystem ////
GemexSystem::GemexSystem(){
	cards.reserve(NGEMEXCARDS);
	ncards=0;
	Print();

	printf("config:\n");
	for(int i=0;i<4;i++)
	for(int j=0;j<16;j++){
		printf("%d %d %d\n",i,j,Gemex::sfp_config[i][j]);
	}
	
	memset(this->config,-1,sizeof(config));
	for(int i=0;i<4;i++)
		for(int j=0;Gemex::sfp_config[i][j]>0;j++){
		   printf("sfp = %d, exp = %d\n",i,j);
			config[i][j]=ncards;
			cards.push_back(GemexData(i,j));
			//ncards+=Gemex::sfp_config[i][j]/2; //2 nxy per card
			ncards+=1; //2 nxy per card
			}

	if(ncards==0){
		printf("gemex syste: 0 cards found !!!\n");
		}	
	Print();
	}
	
GemexSystem::~GemexSystem(){
	}

void GemexSystem::Print(){
	printf("------- Gemex System Info --------\n");
	printf("Number of cards = %d\n",ncards);
	printf("SFP Nxy configuration: \n");
	for(int i=0;i<4;i++){		
		for(int j=0;j<16;j++){
			printf("%d ",Gemex::sfp_config[i][j]);
			}
		printf("\n");
		}
	
	printf("Gemex IDs:\n");			
	for(int i=0;i<4;i++)
		for(int j=0;j<16;j++){
			if(config[i][j]>-1)
				printf("ID = %d, SFP=%d, EXP=%d\n",config[i][j],i,j);
			}			
	}
	
void GemexSystem::LoadBaselineFromFile(char *fname){
	ifstream fr;
	std::string line;
	int s,e,n,c,v;
	fr.open(fname,ifstream::in);
	
	while(fr.good()){
		getline(fr,line);
		if(sscanf(line.c_str(),"%d %d %d %d %d",&s, &e, &n, &c, &v)!=5){
			printf("baseline reading error: could not parse line %s in file %s\n ",line.c_str(),fname);
			continue;
			}
		cards[config[s][e]].baseline[c+(128*n)]=v;
		}
	
	#ifdef GEMEX_PRINT
	printf("\nLoaded baselines:\n");
	for(int i=0;i<ncards;i++){
		for(int j=0;j<256;j++){
			printf("%d %d\n",i,cards[i].baseline[j]);
			}
		}
	#endif
	
	}	
	
	
#ifdef GEMEX_HIS
void GemexSystem::MakeHist(char *dirname){
	char text[30];	
	char path[50];	
	
	for(int i=0;i<ncards;i++){
		sprintf(text,"ADC_S%d_E%d",cards[i].GetSFP(), cards[i].GetEXP());
		sprintf(path,"%s/%s",dirname,text);		
		cards[i].hadc = (TH2I*)TGo4Analysis::Instance()->MakeTH2('I', path, text, 256,0,256,000,-500,3500,"channel [ch]", "ADC [ch]");		
		cards[i].hadc->SetOption("colz");
		
		sprintf(text,"ADC_RAW_S%d_E%d",cards[i].GetSFP(), cards[i].GetEXP());
		sprintf(path,"%s/%s",dirname,text);		
		cards[i].hadc_raw = (TH2I*)TGo4Analysis::Instance()->MakeTH2('I', path, text, 256,0,256,1000,0,4000,"channel [ch]", "ADC [ch]");		
		cards[i].hadc_raw->SetOption("colz");
		
		sprintf(text,"hitpattern_S%d_E%d",cards[i].GetSFP(), cards[i].GetEXP());
		sprintf(path,"%s/%s",dirname,text);		
		cards[i].hhit = (TH1I*)TGo4Analysis::Instance()->MakeTH1('I', path, text, 256,0,256,"channel [ch]", "hits");		
		cards[i].hhit->SetOption("colz");	
		/*
		sprintf(text,"time_S%d_E%d",cards[i].GetSFP(), cards[i].GetEXP());
		sprintf(path,"%s/%s",dirname,text);		
		cards[i].htime = (TH2I*)TGo4Analysis::Instance()->MakeTH2('I', path, text, 256,0,256,4000,0,4000,"channel [ch]", "time [ch]");		
		cards[i].htime->SetOption("colz");
		*/
		}
	printf("GEMEX histograms created\n");
	}
	
void GemexSystem::FillHist(){
	
	for(int i=0;i<ncards;i++){
		for(int j=0;j<cards[i].GetNData();j++){
			cards[i].hadc_raw->Fill(cards[i].GetData(j)->ch, cards[i].GetData(j)->adc);
			cards[i].hadc->Fill(cards[i].GetData(j)->ch, cards[i].baseline[cards[i].GetData(j)->ch]-cards[i].GetData(j)->adc);
			cards[i].hhit->Fill(cards[i].GetData(j)->ch);
			}
		}
	}
#endif

void GemexSystem::ClearHits(){
	for(int i=0;i<ncards;i++){
		cards[i].Clear();
		}
	}

int GemexSystem::Unpack(unsigned int* pdata){
	
	int first, second;
	int nxy_n_cha;
	int _time;
	int _ch;
	int _adc;
	int index;
	
	if((*pdata)==0xbad00bad){
		#ifdef GEMEX_PRINT
		printf("gemex unpack: bad event \n");
		#endif
		return 0;
		}
	
	//clearing previous hits
	ClearHits();
	
	for(int i=0;i<4;i++) //sfp loop
	{
   	if(Gemex::sfp_config[i][0]<1)continue;
		//skip padding		
		for(int j=0;j<100;j++){
			first = *pdata++;
			if( (first&0xfff00000) != 0xadd00000){
				pdata--;
				break;
				}
			}
		
		for(int j=0;Gemex::sfp_config[i][j]>0;j++) // exp loop
		{
			for(int k=0;k<Gemex::sfp_config[i][j];k++){ //nxyter loop
				//exploder header
				first = *pdata++;
				if( ((first & 0xff)>>0)!=0x34){
					#ifdef GEMEX_PRINT
					printf("gemex unpack: channel header is not 0x34 (%x)\n",first);
					#endif
					return 0;
					}
					
				if( ((first & 0xff0000) >> 16) != j )
				{
					#ifdef GEMEX_PRINT
            		printf ("gemex unpack: exploder id is wrong \n");
            		#endif
            		return 0;
          		}
          		
          		if( ((first & 0xff000000) >> 24) != k )
				{
					#ifdef GEMEX_PRINT
            		printf ("gemex unpack: Nxyter id is wrong \n");
            		#endif
            		return 0;
          		}
				
				///data size 
				first = *pdata++; 
				nxy_n_cha = (first - 20) >> 3;
				
				///nxy header
				first = *pdata++;
				if( ((first & 0xff000000) >> 24) != 0xaa)
				{
					#ifdef GEMEX_PRINT
            		printf ("gemex unpack: Nxyter header id is not 0xaa \n");
            		#endif
            		return 0;
          		}
          		
          		////////////////////////////////////////////////////////
				/// trigger counter from nxy header can be read here ///
				////////////////////////////////////////////////////////				
				first = *pdata++;
				second = *pdata++;				
				////////////////////////////////////////////////////////
				/// epoch time should be read here ///
				////////////////////////////////////////////////////////
				
				for(int l=0;l<nxy_n_cha;l++){ //loop over channels
					first = *pdata++;
					second = *pdata++;
					_time = (first & 0x3fff);
					_adc = ((first & 0xfff0000) >> 16);
					_ch = ((second & 0x7f000000) >> 24);
					if(k==1)_ch+=128;
					
					index = config[i][j];
					cards[index].Insert(GemexHit(_ch,_adc,_time));
					
					}//end of channel loop
				//error pattern
				first = *pdata++;
				if((first&0xff000000) != 0xee000000){
				    #ifdef GEMEX_PRINT
            		printf ("gemex unpack: Nxyter error pattern is not 0xee \n");
            		#endif
            		return -1;
				    }
				//nxy trailer
				first = *pdata++;
				if((first&0xff000000) != 0xbb000000){
					#ifdef GEMEX_PRINT
            		printf ("gemex unpack: Nxyter trailer is not 0xbb \n");
            		#endif
            		return -1;
					}
				
				
				}//end of nxy loop
			
			}//end of exp loop
		
		}//end of sfp loop
	
	return 1;
	}

} //end of namespace Gemex
