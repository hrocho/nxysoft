/**
 * @file nxysync.h
 * A.Prochazka
 * 16.10.2013
 * function related to synchronisation over nxyters
 * this should be used under linux where the MBS and nxyter daemon is runnig
 */ 
#ifndef NXYSYNC_H
#define NXYSYNC_H

#include <fcntl.h>
#include <sys/stat.h>
#include <semaphore.h>

#define NXY_SEMNAME "nxy_sem" /**< linux semaphore name */
#define NXY_SHMNAME "nxy_shm" /**< linux shared memory name */
#define NXY_MAX 32 /**< maximum number of nxyter used*/


sem_t *nxy_mutex; /**< pointer to the linux semaphore */
char  *nxy_shm; /**< poiner to the linux shared memory */

/**
 * Initializes the semaphore and shared memory for MBS daemon synchronization
 * @return 1 on success, -1 on failure
 */ 
int nxy_init_ipc();

/**
 * Cleans and destroys the semaphore and shared memory used for MBS daemon synchronization
 */ 
void nxy_clear_ipc();

/**
 * Lock the nxyter operation
 * The function wait until the nxyter is unlocked by other programs (MBS, nxyter daemon etc)
 * @return 0 on success, -1 on failure
 */ 
int nxy_lock();

/**
 * Lock the nxyter operation, if the lock can not be obtained soon the lock will be released and acquired by the function
 * The function wait some time and if the nxyter is still locked by other programs (MBS, nxyter daemon etc), the function 
 * will release and lock the nxyter lock
 * @return 0 on success, -1 on failure
 */ 
int nxy_force_lock();

/**
 * Unlock the nxyter operation
 * The function will unlock the nxyter operation so other other programs (MBS, nxyter daemon etc) can lock it
 * @return 0 on success, -1 on failure
 */ 
int nxy_unlock();

/**
 * Returns the number indication the status of locking of nxyter operation
 * 0 means nxyter is locked, 1 means nxyter is free
 */ 
int nxy_getlockvalue();

#endif
