/**
 * @file
 * A.Prochazka
 * 13.7.2013
 * here are the functions used to manipulate with global and nxyter registers
 * including function for communicating with the nxyter daemon
 * 
 */ 
#include "nxyreg.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sys/socket.h"
#include <netinet/in.h>
#include <sys/types.h>
#include <netdb.h>
#include <arpa/inet.h>


/// helping functions for load_nxy_file
int get_nxyreg_index(struct nxyreg *reg, int sfp, int exp, int nxy, int max);
int get_nxy_number(char *filename);


/// return nxy index accorfing to global reg config and sfp, exp, nxy number
int nxy_map(int sfp, int exp, int nxy, struct globalreg greg){
    int i=0,j=0;
    int index = -1;

    for(i=0;i<MAX_SFP;i++)
        for(j=0;j<MAX_EXP;j++){
            if(greg.sfp_in_use[i][j]==0)break;
            if(sfp==i && exp==j){
                if(nxy==0){index++;return index;}
                if(nxy==1){index+=2;return index;}
            }
            index+=greg.sfp_in_use[i][j];
    }

    return -1;
}


int nxy_id(int index, int *sfp, int *exp, int *nxy, struct globalreg greg){
	int i,j,k;
	int counter=0;

	for(i=0;i<MAX_SFP;i++)
        for(j=0;j<MAX_EXP;j++){
		if(greg.sfp_in_use[i][j]==0)break;
		for(k=0;k<greg.sfp_in_use[i][j];k++)
		{
			if(index == counter){
				*sfp = i;
				*exp = j;
				*nxy = k;
				return 1;
			}
			counter++;
		}
	}
	return 0;
}
/*
int nxyreg_prepare(struct nxyreg *r, struct globalreg greg){
	int i;	
	r = (struct nxyreg*)malloc(greg.nxy_number*sizeof(struct nxyreg));
	if(r==NULL)return 0;
	
	for(i=0;i<greg.nxy_number;i++)
	{
		if(nxy_id(i,&r[i].sfp,&r[i].exp,&r[i].nxy,greg)==0)return 0;
	}

	return 1;
}
*/
/// copy nxyreg r0 to r and globalreg g0 to g
void* nxyregcpy(struct nxyreg **r, struct nxyreg **r0, struct globalreg *g, struct globalreg *g0){
	if(*r==NULL){
		printf("allocating for %d \n",g0->nxy_number*sizeof(struct nxyreg));
		*r = (struct nxyreg*)malloc(g0->nxy_number*sizeof(struct nxyreg));
	}
	if(*r==NULL) return NULL;
	memcpy(g,g0,sizeof(struct globalreg));
	memcpy(*r,*r0,g0->nxy_number*sizeof(struct nxyreg));
	return *r;
}

/// get number of exploders at sfp, from globalreg data
int get_explo_number(struct globalreg greg, int sfp){
    int i=0;
    int res = 0;
    for(i=0;i<MAX_EXP;i++){
        if(greg.sfp_in_use[sfp][i]==0)break;
    }


    return i;
}

int get_nxyreg_index(struct nxyreg *reg, int sfp, int exp, int nxy, int max)
{
	int index=-1;
	int i;
	for(i=0;i<max;i++)
		{
			if(reg[i].sfp==sfp && reg[i].exp==exp && reg[i].nxy==nxy)
				{index = i; break;}
			}
	return index;
	}
///////////////////////////////////////////////////////////////////////
int get_nxy_number(char *filename)
{
  char c_input[200];
  FILE *fr;
  char c_sfpx[16];
  int32_t l_par[16];
  int nxycount=0;
  int i,j,l,result;


  if ((fr = fopen (filename, "r")) == NULL)
  {
  	printf ("input file %s not found \n", filename);
  	exit(1);
  }


  j=0;
  while (fgets (c_input, 200, fr) != NULL)
	{
		if(strlen(c_input)>1 && c_input[0]!='#')
			{
				memset(l_par,0,sizeof(l_par));

				if(strstr(c_input,"SFP")!=NULL &&strstr(c_input,"EXP_IN_USE"))
				{
					result = sscanf (c_input, "%s %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d", c_sfpx,
                				&l_par [0], &l_par [1], &l_par [2], &l_par [3],
                				&l_par [4], &l_par [5], &l_par [6], &l_par [7],
                				&l_par [8], &l_par [9], &l_par[10], &l_par[11],
                				&l_par[12], &l_par[13], &l_par[14], &l_par[15]);

					if(result == 17)
						{
							l=c_sfpx[3]-'0';
							for (i=0; i<MAX_EXP; i++){nxycount+= l_par[i] & 0x3;}
							j++;
							}
					else
						{
							printf("error in line : %s",c_input);return 0;
							}
				}
				} //end of if strlen > 1 && c_input[0]!=#
		if(j>4)break;
		}  //end of while fgets

	return nxycount;
}

/// Load nxyter and exploder setting from txt file
/// the function allocate memory for nxyreg, user should free it
struct nxyreg* load_nxy_file(char *filename, struct globalreg *greg)
{
  struct nxyreg *r=NULL;
  char c_input[200];
  FILE *fr;
  char c_sfp[4];
  char c_exp[4];
  char c_nxy[4];
  char c_par[20];


  int32_t l_sfp;
  int32_t l_exp;
  int32_t l_nxy;
  int32_t l_par[16];

  int nxycount=0;
  int index=-1;
  int i,l,result;
  int check = 0;
  int *regcheck = NULL;

  if ((fr = fopen (filename, "r")) == NULL)
  {
  	printf ("input file %s not found \n", filename);
  	return NULL;
  }

  	nxycount = 0;
  	//reading global settings
  	while(fgets(c_input,200,fr) != NULL){

  		if(strlen(c_input)<3)continue;
  		if(c_input[0]=='#')continue;
		memset(c_par,0,sizeof(c_par));
		memset(l_par,0,sizeof(l_par));

		if(strstr(c_input,"SFP")!=NULL && strstr(c_input,"EXP_IN_USE")){
			result = sscanf (c_input, "%s %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d", c_par,
                				&l_par [0], &l_par [1], &l_par [2], &l_par [3],
                				&l_par [4], &l_par [5], &l_par [6], &l_par [7],
                				&l_par [8], &l_par [9], &l_par[10], &l_par[11],
                				&l_par[12], &l_par[13], &l_par[14], &l_par[15]);
            if(result==17){
            	l = c_par[3]-'0';
            	if(l<0 || l>3){printf("parsing error: %s",c_input);return NULL;}
            	check += (1<<l);
            	for(i=0;i<MAX_EXP;i++){
            		greg->sfp_in_use[l][i]=l_par[i]&0x3;
            		if(greg->sfp_in_use[l][i]==2)nxycount+=2; //counting active nxyters
            		if(greg->sfp_in_use[l][i]==1)nxycount++; //for feb board, for gemex can be removed
            		}
            	}
            else{
            	printf("error in line: %s",c_input);return NULL;
            	}
			} //end of SFP EXP_IN_USE search

		if(strstr(c_input,"GLOBAL_PARAM")!=NULL){
			result = sscanf(c_input,"%s %x %x %x %x",c_par, &l_par[0], &l_par[1], &l_par[2], &l_par[3]);
			if(result == 5){
				check += 16;
				greg->pre_trg_wind = l_par[0]&0xfff;
				greg->pos_trg_wind = l_par[1]&0xfff;
				greg->test_pul_del = l_par[2]&0xff;
				greg->test_trg_del = l_par[3]&0xff;
				}
			else{
				printf("error in line: %s");return NULL;
				}
			}
		if(check==31)break;
  		} // end of while fgets for global setting

  	if(check!=31){
  		printf("error: could not load complete system config or global parameters	\n");return NULL;
  		}
  	greg->nxy_number = nxycount;
  	//printf("allocating for  %d nxy\n",greg->nxy_number);


  	// memory allocation for nxyters settings
  	regcheck = (int *)malloc(nxycount*sizeof(int));
  	r = (struct nxyreg *)malloc(nxycount*sizeof(struct nxyreg));
  	memset(regcheck,0,nxycount*sizeof(int));
  	memset(r,0,nxycount*sizeof(struct nxyreg));
  	if(r==NULL){printf("allocation failed");exit(1);}
  	//indexing nxyregister arrays to sfp exp nxy config
  	nxycount=0;

  	for(l=0;l<MAX_SFP;l++)
    for(i=0;i<MAX_EXP;i++){
        if(greg->sfp_in_use[l][i]==2){
        	r[nxycount].sfp=l;
            r[nxycount].exp=i;
            r[nxycount].nxy=0;
            nxycount++;
            r[nxycount].sfp=l;
            r[nxycount].exp=i;
            r[nxycount].nxy=1;
            nxycount++;
        }

    }

  	while (fgets (c_input, 200, fr) != NULL)
	{
		//printf("\nre4 = %d, %s \n",r,c_input);
		if(strlen(c_input)<3)continue;
		if(c_input[0]=='#')continue;

		memset(c_sfp,0,sizeof(c_sfp));
		memset(c_exp,0,sizeof(c_exp));
		memset(c_nxy,0,sizeof(c_nxy));
		memset(c_par,0,sizeof(c_par));
		memset(l_par,0,sizeof(l_par));

		if(strstr(c_input,"SFP")!=NULL && strstr(c_input,"EXP")!=NULL && strstr(c_input,"NXY")!=NULL)
			{
			result = sscanf (c_input, "%s %d %s %d %s %d %s %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x",
             							c_sfp, &l_sfp, c_exp, &l_exp, c_nxy, &l_nxy, c_par,
             							&l_par [0], &l_par [1], &l_par [2], &l_par [3],
             							&l_par [4], &l_par [5], &l_par [6], &l_par [7],
             							&l_par [8], &l_par [9], &l_par[10], &l_par[11],
             							&l_par[12], &l_par[13], &l_par[14], &l_par[15]);

			if(l_sfp>3 || l_sfp<0 || l_exp>15 || l_exp<0 || l_nxy<0 || l_nxy>1)break;
			index = get_nxyreg_index(r,l_sfp,l_exp,l_nxy,greg->nxy_number);
			if(index<0){printf("error: unidentified nxyter\n");free(regcheck);return NULL;}
            if((strcmp (c_sfp, "SFP") == 0) && (strcmp (c_exp, "EXP") == 0) && strcmp(c_nxy,"NXY")==0)
            	{
            	///
             	if ((strcmp (c_par, "I2C_ADDR")) == 0 && (result ==8))
             		{
             			r[index].i2c_addr = l_par[0] & 0xff;
             			regcheck[index] += 1;
           				}
           		///
             	else if ((strcmp (c_par, "RESET")) == 0 && (result ==8))
             		{
             			r[index].reset = l_par[0] & 0xff;
             			regcheck[index] += 2;
           				}
           		///
             	else if ((strcmp (c_par, "MASK")) == 0 && (result ==23))
             		{
             			for (i=0; i<16; i++)r[index].mask[i] = l_par[i] & 0xff;
             			regcheck[index] += 4;
           				}
           		///
            	else if ((strcmp (c_par, "BIAS")) == 0 && (result ==21))
            		{
            			for (i=0; i<14; i++)r[index].bias[i] = l_par[i] & 0xff;
            			regcheck[index] += 8;
           				}
           		///
           		else if ((strcmp (c_par, "CONFIG")) == 0 && (result ==9))
             		{
             			r[index].config[0] = l_par[0] & 0xff;
             			r[index].config[1] = l_par[1] & 0xff;
             			regcheck[index] += 16;
           				}
           		///
           		else if ((strcmp (c_par, "TEST_DELAY")) == 0 && (result ==9))
             		{
             			r[index].te_del[0] = l_par[0] & 0xff;
             			r[index].te_del[1] = l_par[1] & 0xff;
             			regcheck[index] += 32;
           				}
           		///
           		else if ((strcmp (c_par, "CLOCK_DELAY")) == 0 && (result ==10))
             		{
             			r[index].clk_del[0] = l_par[0] & 0xff;
             			r[index].clk_del[1] = l_par[1] & 0xff;
             			r[index].clk_del[2] = l_par[2] & 0xff;
             			regcheck[index] += 64;
           			}
           		///
             	else if ((strcmp (c_par, "THR_TEST")) == 0 && (result ==8))
             		{
             			r[index].thr_te = l_par[0] & 0xff;
             			regcheck[index] += 128;
           			}
           		///
             	else if ((strcmp (c_par, "THR_0_15")) == 0 && (result ==23))
             		{
             			for (i=0; i<16; i++)r[index].thr[i] = l_par[i] & 0xff;
             			regcheck[index] += 256;
           			}
           		///
            	else if ((strcmp (c_par, "THR_16_31")) == 0 && (result ==23))
            		{
            			for (i=0; i<16; i++)r[index].thr[i+16] = l_par[i] & 0xff;
            			regcheck[index] += 512;
           			}
           		///
             	else if ((strcmp (c_par, "THR_32_47")) == 0 && (result ==23))
             		{
             			for (i=0; i<16; i++)r[index].thr[i+32] = l_par[i] & 0xff;
             			regcheck[index] += 1024;
           			}
           		///
             	else if ((strcmp (c_par, "THR_48_63")) == 0 && (result ==23))
             		{
             			for (i=0; i<16; i++)r[index].thr[i+48] = l_par[i] & 0xff;
             			regcheck[index] += 2048;
           			}
           		///
           		else if ((strcmp (c_par, "THR_64_79")) == 0 && (result ==23))
             		{
             			for (i=0; i<16; i++)r[index].thr[i+64] = l_par[i] & 0xff;
             			regcheck[index] += 4096;
           			}
           		///
           		else if ((strcmp (c_par, "THR_80_95")) == 0 && (result ==23))
             		{
             			for (i=0; i<16; i++)r[index].thr[i+80] = l_par[i] & 0xff;
             			regcheck[index] += 8192;
           			}
           		///
             	else if ((strcmp (c_par, "THR_96_111")) == 0 && (result ==23))
             		{
             			for (i=0; i<16; i++)r[index].thr[i+96] = l_par[i] & 0xff;
             			regcheck[index] += (1<<14);
           			}
				///
             	else if ((strcmp (c_par, "THR_112_127")) == 0 && (result ==23))
             		{
             			for (i=0; i<16; i++)r[index].thr[i+112] = l_par[i] & 0xff;
             			regcheck[index] += (1<<15);
           			}
           		///
             	else if ((strcmp (c_par, "ADC_DCO_PHASE")) == 0 && (result ==8))
             		{
             			r[index].adc_dco_phase = l_par[0] & 0xff;
             			regcheck[index] += (1<<16);
           			}
           		else {
           			printf("error in line : %s",c_input);free(regcheck); return NULL;
           			}


             }//end if SFP EXP NXY string where expected
             else {printf("error in line : %s",c_input);free(regcheck); return NULL;}


			}//end if SFP EXP NXY in c_ipnut



		}  //end of while fgets

	check = 1;
	for(i=0;i<nxycount;i++){
		if(regcheck[i] != 0x1ffff)check*=0;
		}
    if(check == 0){printf("error: incomplete nxyter registers config\n");free(regcheck); return NULL;}
    free(regcheck);
	return r;
}

/// print the nxyter settings to stdout
void print_nxy_reg(struct nxyreg reg)
{
	int i;
	printf("\nnxyter: SFP = %d, EXP = %d, NXY = %d",reg.sfp,reg.exp,reg.nxy);
	printf("\nI2C_ADDR: ");
    printf("0x%x ",reg.i2c_addr);

    printf("\nRESET: ");
    printf("0x%x ",reg.reset);

	printf("\nMASK: ");
	for(i=0;i<16;i++)printf("0x%02x ",reg.mask[i]);

	printf("\nBIAS: ");
	for(i=0;i<14;i++)printf("0x%02x ",reg.bias[i]);

	printf("\nCONFIG: ");
	for(i=0;i<2;i++)printf("0x%02x ",reg.config[i]);

	printf("\nTEST_DELAY: ");
	for(i=0;i<2;i++)printf("0x%02x ",reg.te_del[i]);

	printf("\nCLOCK_DELAT: ");
	for(i=0;i<3;i++)printf("0x%02x ",reg.clk_del[i]);

	printf("\nTHR_TEST: ");
    printf("0x%02x ",reg.thr_te);

	printf("\nTHR_0_15: ");
	for(i=0;i<16;i++)printf("0x%02x ",reg.thr[i]);

    printf("\nTHR_16_31: ");
	for(i=0;i<16;i++)printf("0x%02x ",reg.thr[i+16]);

	printf("\nTHR_32_47: ");
	for(i=0;i<16;i++)printf("0x%02x ",reg.thr[i+32]);

	printf("\nTHR_48_63: ");
	for(i=0;i<16;i++)printf("0x%02x ",reg.thr[i+48]);

	printf("\nTHR_64_79: ");
	for(i=0;i<16;i++)printf("0x%02x ",reg.thr[i+64]);

	printf("\nTHR_80_95: ");
	for(i=0;i<16;i++)printf("0x%02x ",reg.thr[i+80]);

	printf("\nTHR_96_111: ");
	for(i=0;i<16;i++)printf("0x%02x ",reg.thr[i+96]);

    printf("\nTHR_112_127: ");
	for(i=0;i<16;i++)printf("0x%02x ",reg.thr[i+112]);

	printf("\nADC_DCO_PHASE: ");
    printf("0x%02x ",reg.adc_dco_phase);
    printf("\n");

}

/// Print exploder settings to stdout
void print_global(struct globalreg greg)
{
int i,j;
printf("\nnumber of nxyters: %d",greg.nxy_number);
printf("\nGLOBAL_PARAM 0x%02x 0x%02x 0x%02x  0x%02x",greg.pre_trg_wind,greg.pos_trg_wind,greg.test_pul_del,greg.test_trg_del);

for(i=0;i<MAX_SFP;i++)
	{
	printf("\nSFP%d_EXP_IN_USE",i);
	for(j=0;j<MAX_EXP;j++)
		{
		printf(" %d",greg.sfp_in_use[i][j]);
		}
	}
printf("\nmode = 0x%x\n\n",greg.mode);
}

/// Save nxyter and exploder settings to txt file
int save_nxy_file(char *name, struct globalreg greg, struct nxyreg *r)
{
	int i,j;
	FILE *fr = fopen(name,"w");
	if(fr==NULL){return 0;}
	if(greg.nxy_number<0 || greg.nxy_number>99999){return -1;}

	// printing global parameters:
	fprintf(fr,"# nxyter used = %d \n",greg.nxy_number);

	for(i=0;i<MAX_SFP;i++)
	{
		fprintf(fr,"\nSFP%d_EXP_IN_USE",i);
		for(j=0;j<MAX_EXP;j++)
			{
			fprintf(fr," %d",greg.sfp_in_use[i][j]);
			}
	}
	fprintf(fr,"\nGLOBAL_PARAM 0x%02x 0x%02x 0x%02x  0x%02x",greg.pre_trg_wind,greg.pos_trg_wind,greg.test_pul_del,greg.test_trg_del);


	//printing nxyters settings
	for(i=0;i<greg.nxy_number;i++)
	{
		fprintf(fr,"\n# nxyter: SFP = %d, EXP = %d, NXY = %d",r[i].sfp,r[i].exp,r[i].nxy);

		fprintf(fr,"\nSFP %d EXP %d NXY %d",r[i].sfp,r[i].exp,r[i].nxy);
		fprintf(fr," I2C_ADDR \t");
        fprintf(fr,"0x%02x ",r[i].i2c_addr);

        fprintf(fr,"\nSFP %d EXP %d NXY %d",r[i].sfp,r[i].exp,r[i].nxy);
        fprintf(fr," RESET \t");
    	fprintf(fr,"0x%02x ",r[i].reset);

    	fprintf(fr,"\nSFP %d EXP %d NXY %d",r[i].sfp,r[i].exp,r[i].nxy);
    	fprintf(fr," MASK \t");
		for(j=0;j<16;j++)fprintf(fr,"0x%02x ",r[i].mask[j]);

		fprintf(fr,"\nSFP %d EXP %d NXY %d",r[i].sfp,r[i].exp,r[i].nxy);
		fprintf(fr," BIAS \t");
		for(j=0;j<14;j++)fprintf(fr,"0x%02x ",r[i].bias[j]);

		fprintf(fr,"\nSFP %d EXP %d NXY %d",r[i].sfp,r[i].exp,r[i].nxy);
		fprintf(fr," CONFIG \t");
		for(j=0;j<2;j++)fprintf(fr,"0x%02x ",r[i].config[j]);

		fprintf(fr,"\nSFP %d EXP %d NXY %d",r[i].sfp,r[i].exp,r[i].nxy);
		fprintf(fr," TEST_DELAY \t");
		for(j=0;j<2;j++)fprintf(fr,"0x%02x ",r[i].te_del[j]);

		fprintf(fr,"\nSFP %d EXP %d NXY %d",r[i].sfp,r[i].exp,r[i].nxy);
		fprintf(fr,	" CLOCK_DELAY \t");
		for(j=0;j<3;j++)fprintf(fr,"0x%02x ",r[i].clk_del[j]);

		fprintf(fr,"\nSFP %d EXP %d NXY %d",r[i].sfp,r[i].exp,r[i].nxy);
		fprintf(fr," THR_TEST \t");
    	fprintf(fr,"0x%02x ",r[i].thr_te);


    	fprintf(fr,"\nSFP %d EXP %d NXY %d",r[i].sfp,r[i].exp,r[i].nxy);
		fprintf(fr," THR_0_15 \t");
		for(j=0;j<16;j++)fprintf(fr,"0x%02x ",r[i].thr[j]);

		fprintf(fr,"\nSFP %d EXP %d NXY %d",r[i].sfp,r[i].exp,r[i].nxy);
    	fprintf(fr," THR_16_31 \t");
		for(j=0;j<16;j++)fprintf(fr,"0x%02x ",r[i].thr[j+16]);

		fprintf(fr,"\nSFP %d EXP %d NXY %d",r[i].sfp,r[i].exp,r[i].nxy);
		fprintf(fr," THR_32_47 \t");
		for(j=0;j<16;j++)fprintf(fr,"0x%02x ",r[i].thr[j+32]);

		fprintf(fr,"\nSFP %d EXP %d NXY %d",r[i].sfp,r[i].exp,r[i].nxy);
		fprintf(fr," THR_48_63 \t");
		for(j=0;j<16;j++)fprintf(fr,"0x%02x ",r[i].thr[j+48]);

		fprintf(fr,"\nSFP %d EXP %d NXY %d",r[i].sfp,r[i].exp,r[i].nxy);
		fprintf(fr," THR_64_79 \t");
		for(j=0;j<16;j++)fprintf(fr,"0x%02x ",r[i].thr[j+64]);

		fprintf(fr,"\nSFP %d EXP %d NXY %d",r[i].sfp,r[i].exp,r[i].nxy);
		fprintf(fr," THR_80_95 \t");
		for(j=0;j<16;j++)fprintf(fr,"0x%02x ",r[i].thr[j+80]);

		fprintf(fr,"\nSFP %d EXP %d NXY %d",r[i].sfp,r[i].exp,r[i].nxy);
		fprintf(fr," THR_96_111 \t");
		for(j=0;j<16;j++)fprintf(fr,"0x%02x ",r[i].thr[j+96]);

    	fprintf(fr,"\nSFP %d EXP %d NXY %d",r[i].sfp,r[i].exp,r[i].nxy);
    	fprintf(fr," THR_112_127 \t");
		for(j=0;j<16;j++)fprintf(fr,"0x%02x ",r[i].thr[j+112]);

		fprintf(fr,"\nSFP %d EXP %d NXY %d",r[i].sfp,r[i].exp,r[i].nxy);
		fprintf(fr," ADC_DCO_PHASE \t");
    	fprintf(fr,"0x%02x ",r[i].adc_dco_phase);
    	fprintf(fr,"\n");
	}


	fclose(fr);

	return 1;
	}

/// save nxyter and exploder settings to binary file
int save_bin_file(char *name, struct globalreg greg, struct nxyreg *r){
	FILE *fw = fopen(name,"wb");
	if(fw==NULL){return 0;}
	fwrite((char *)&greg,sizeof(struct globalreg),1,fw);
	fwrite((char *)r,greg.nxy_number*sizeof(struct nxyreg),1,fw);
	fclose(fw);
	return 1;
	}

/// load nxyter and exploder settings from binary file
struct nxyreg* load_bin_file(char *name, struct globalreg *greg){
	struct nxyreg *r;
	FILE * fr = fopen(name,"rb");
	if(fr==NULL){return 0;}
	memset(greg,0,sizeof(greg));
	fread(greg,sizeof(struct globalreg),1,fr);
	printf("allocating for  %d nxy\n",greg->nxy_number);
	r = (struct nxyreg *)malloc(greg->nxy_number*sizeof(struct nxyreg));
	memset(r,0,greg->nxy_number*sizeof(struct nxyreg));
	if(r==NULL)return NULL;
	fread(r,greg->nxy_number*sizeof(struct nxyreg),1,fr);


	fclose(fr);
	return r;
	}

///////////////////  Socket helping function //////////////////////////

int sendall(int s, char *buf, int *len)
{
    int total = 0;        // how many bytes we've sent
    int bytesleft = *len; // how many we have left to send
    int n;

    while(total < *len) {
        n = send(s, buf+total, bytesleft, 0);
        if (n == -1) { break; }
        total += n;
        bytesleft -= n;
    }

    *len = total; // return number actually sent here

    return n==-1?-1:1; // return -1 on failure, 1 on success
}

int recvall(int s, char *buf, int len){
    int total = 0;

    int n = 0;

    while(total<len){
        n = recv(s,buf,len-total,0);
        if(n<0)break;
        total+=n;
        buf+=n;
    }
//    if(total!=len)total = -1;
    return total;
    }

/// function to pack the data stream into header and trailers and prepare it before sending
char* pack(int com, int *length, char *data)
{
    int len;
    int dummy;
    char *buf = NULL;
    char *save=NULL;
    if(data==NULL)*length=0;
    len = *length+4*(sizeof(int)); //data + header + tail + command

    //alocation of memory for sending
    buf = (char*)malloc(len);
    if(buf==NULL){return NULL;}

    //header
    dummy = 0xab00;
    dummy+=sizeof(int32_t);
    save = buf;

    memcpy(save,&dummy,sizeof(int));
    save+=sizeof(int);

    //data size
    memcpy(save,length,sizeof(int));
    save+=sizeof(int);

    //command
    memcpy(save,&com,sizeof(int));
    save+=sizeof(int);

    //data itself
    if(*length>0){
        memcpy(save,data,*length);
        save+=*length;
    }

    //tail
    dummy=0xe;
    memcpy(save,&dummy,sizeof(int));
    save+=sizeof(int);

    if((save-buf)!=len){free(buf);buf=NULL;return NULL;}
    *length = len;
    return buf;
}

/// function to read data stream from fd file descriptor (network socket) and check headers and trailers
char* unpackdata(int fd,int *len,int *com){
    int i[3];
    int length;
    char *buf=NULL;
    *com = 0;
    length = recv(fd,i,3*sizeof(int),0);
	if(length!=(3*sizeof(int))){return NULL;}
	if(i[0]!=0xab04){return NULL;} //header check
	if(i[1]<0){return NULL;}
    *len = i[1]+sizeof(int);

    buf = (char*)malloc(i[1]+sizeof(int)); //data size + tail
    memset(buf,0,i[1]);
    length = recvall(fd,buf,i[1]+sizeof(int));
    if(length!=*len)return NULL;
    *com = i[2];
    if(*(buf+i[1])!=0xe)return NULL;
    return buf;
}

/// convert nxyter and exp. configs to data stream
char* nxydata2str(struct globalreg greg, struct nxyreg *r,int *len)
{
    int length=0;
    char *buf=NULL;

    length = sizeof(struct globalreg)+(greg.nxy_number*sizeof(struct nxyreg));

    buf = (char*)malloc(length);
    if(buf==NULL)return NULL;

    memcpy(buf,&greg,sizeof(greg));
    memcpy(buf+sizeof(greg),r,greg.nxy_number*sizeof(struct nxyreg));

    *len = length;
    return buf;
}

/// convert data stream to xnyter and expl. data
struct nxyreg* str2nxydata(char* buf, struct globalreg *greg)
{
    struct nxyreg *r=NULL;
    if(buf == NULL)return NULL;
    memcpy(greg,buf,sizeof(struct globalreg));

    r=(struct nxyreg*)malloc(greg->nxy_number*sizeof(struct nxyreg));
    if(r==NULL)return NULL;
    memcpy(r,buf+sizeof(struct globalreg),greg->nxy_number*sizeof(struct nxyreg));

    return r;
}


int connect_daemon(char *sname){
    struct addrinfo hints, *servinfo,*p;
    int status;
   
    memset(&hints, 0,sizeof(hints));

    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;

    status = getaddrinfo(sname,NXYD_PORT,&hints,&servinfo);
    if(status != 0)return -1;

        for(p=servinfo;p!=NULL;p=p->ai_next){
        nxyd_fd = socket(p->ai_family,p->ai_socktype,p->ai_protocol);
        if(nxyd_fd<0){freeaddrinfo(servinfo);return -1;}
        status = connect(nxyd_fd,p->ai_addr,p->ai_addrlen);
        if(status<0){freeaddrinfo(servinfo);close(nxyd_fd);return -1;}
        break;
    }
    if(p==NULL){freeaddrinfo(servinfo);close(nxyd_fd);return -1;}
    freeaddrinfo(servinfo);

    return nxyd_fd;
}

int close_daemon(){
    int status;
    int com = 0;
    int len;
    //char message[3];
    char *buf=NULL,*save=NULL;

    com  = (int)'B'<<24;
    //save = nxydata2str(greg,r,&len);
    buf = pack(com,&len,save);
    free(save);
    if(buf==NULL)return -1;
    status = sendall(nxyd_fd,buf,&len);
    free(buf);
    close(nxyd_fd);
    return 1;
}


int read_daemon(struct nxyreg **r, struct globalreg *greg){
    int status;
    int com = 0;
    int len;
    char *buf;

    com  = (int)'R'<<24;

    buf = pack(com,&len,NULL);
    if(buf==NULL)return -2;
    status = sendall(nxyd_fd,buf,&len);
    if(status < 1 ){free(buf);return -2;}
    free(buf);

    buf =unpackdata(nxyd_fd,&len,&com);
    if(buf == NULL){return -1;}
    *r = str2nxydata(buf,greg);
    if(*r==NULL){free(buf);return -1;}
    
    free(buf);
    return 1;
}


int update_daemon(struct nxyreg *r, struct globalreg greg,int mode){
    int status;
    int com = 0;
    int len;
    char message[3];
    char *buf=NULL,*save=NULL;

    com  = (int)'w'<<24;
    com += mode;

    save = nxydata2str(greg,r,&len);
    buf = pack(com,&len,save);
    free(save);
    if(buf==NULL)return -2;
    status = sendall(nxyd_fd,buf,&len);
    if(status < 1 ){free(buf);return -2;}
    free(buf);
    status = recv(nxyd_fd,message,3,0);
    if(memcmp(message,(char *)"AOK",3)!=0)return -1;
    return 1;
}

int reset_daemon(struct nxyreg *rr, struct globalreg gg, int testmode){
    int status;
    int com = 0;
    int len;
    char message[3];
    char *buf=NULL,*save=NULL;


    if(testmode){
        com  = (int)'T'<<24;
    }
    else{
       com  = (int)'W'<<24;
    }
	com += (255<<16); //reset
	com += 255;

    save = nxydata2str(gg,rr,&len);
    buf = pack(com,&len,save);
    free(save);
    if(buf==NULL)return -2;
    status = sendall(nxyd_fd,buf,&len);
    if(status < 1 ){free(buf);return -2;}
    free(buf);
    status = recv(nxyd_fd,message,3,0);
    if(memcmp(message,(char *)"AOK",3)!=0)return -1;
    return 1;
}
