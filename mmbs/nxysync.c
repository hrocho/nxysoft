/**
 * @file nxysync.h
 * A.Prochazka
 * 13.7.2013
 * function related to synchronisation over nxyters
 * this should be used under linux where the MBS and nxyter daemon is runnig
 */ 
 
#include "nxysync.h"
#include <unistd.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include "nxyreg.h"
#include <time.h>

int nxy_lock(){
	return sem_wait(nxy_mutex);
	}

int nxy_force_lock(){
	struct timespec ts;
	int waittime = 2;
	int status;
	
	clock_gettime(CLOCK_REALTIME,&ts);
	ts.tv_sec+=waittime;
	status = sem_timedwait(nxy_mutex,&ts);
	
	if(status == 0) return 0;
	if(status!=0){
		nxy_unlock();
		return nxy_lock();
		}
	return sem_wait(nxy_mutex);
	}
	
int nxy_unlock(){
	return sem_post(nxy_mutex);
	}
	
int nxy_getlockvalue(){
	return nxy_mutex->__align;
	}	

int nxy_init_ipc()
{
	int nxy_shm_fd;	
	int nxy_shmsize;
	
	nxy_shmsize = NXY_MAX*sizeof(struct nxyreg) + sizeof(struct globalreg);
	
	nxy_mutex = sem_open(NXY_SEMNAME,O_CREAT,0644,1);
	if(nxy_mutex == SEM_FAILED){
		return -1;
		}	
		
	nxy_shm_fd = shm_open(NXY_SHMNAME,O_RDWR|O_CREAT,0777);
	if(nxy_shm_fd==-1){
		return -1;
		}
		
	ftruncate(nxy_shm_fd,nxy_shmsize);	
	
	nxy_shm = mmap(0,nxy_shmsize,PROT_READ|PROT_WRITE,MAP_SHARED,nxy_shm_fd,0);
	if(nxy_shm==MAP_FAILED){
		return -1;
		}
	close(nxy_shm_fd);
	return 1;	
}

void nxy_clear_ipc()
{
	sem_unlink(NXY_SEMNAME);
	shm_unlink(NXY_SHMNAME);	
}
