/**
 * @file main.cpp
 * GTK GUI for reading, writing and adjusting nxyter registers
 * A. Prochazka a.prochazka@gsi.de
 * last update: 16.10.2014
 *
 * @file main.cpp
 * @author A.Prochazka
 * @brief GTK GUI for reading, writing and adjusting nxyter registers
 * required headers: nxyreg.h
 * 
 */

#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "utils.h"
#include "nxyreg.h"
#include "nxywidget.h"
#include "defs.h"


static void destroy( GtkWidget *widget,
                     gpointer   data )
{
    gtk_main_quit ();
}


int main( int   argc, char *argv[] )
{
    GtkWidget *window;
    GtkWidget *frame;
	/// toolbar variables
    GtkWidget *toolbar;
    GtkToolItem *toolbar_open;
    GtkToolItem *toolbar_save;
    GtkToolItem *toolbar_read;
    GtkToolItem *toolbar_write;
    GtkToolItem *toolbar_quit;
    GtkToolItem *toolbar_sep;

    GtkWidget *vbox;
    GtkWidget *hbox;
    GtkWidget *hbox_nxyselector;
    GtkWidget *table_nxyselector;
    GtkWidget *table;
    GtkWidget *frame1,*frame3;
    GtkWidget *label;
    GtkTooltips *tooltips;
    int yes = 1;
    int no = 0;


    ///init
    global_filename = g_string_sized_new(100);
    gtk_init (&argc, &argv);
    memset(&greg,0,sizeof(struct globalreg));

	/// Nxyter setting widget. It whows number of nxys per sfp
    global_window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    window = global_window;
	gtk_window_set_title(GTK_WINDOW(window),"Nxy setting");
	gtk_window_set_default_size(GTK_WINDOW(window),400,480);
    gtk_container_set_border_width (GTK_CONTAINER (window), 5);

    vbox = gtk_vbox_new(FALSE,0);
    gtk_container_add(GTK_CONTAINER(window),vbox);

    ///  toolbar 
    toolbar = gtk_toolbar_new();
    gtk_toolbar_set_style(GTK_TOOLBAR(toolbar),GTK_TOOLBAR_ICONS);
    gtk_container_set_border_width(GTK_CONTAINER(toolbar),2);

    toolbar_open = gtk_tool_button_new_from_stock(GTK_STOCK_OPEN);
    gtk_toolbar_insert(GTK_TOOLBAR(toolbar),toolbar_open,-1);
    gtk_tool_item_set_tooltip_text(toolbar_open,"Open file");
    
    toolbar_save = gtk_tool_button_new_from_stock(GTK_STOCK_SAVE);
    gtk_toolbar_insert(GTK_TOOLBAR(toolbar),toolbar_save,-1);
    gtk_tool_item_set_tooltip_text(toolbar_save,"Save config");
    
    toolbar_sep = gtk_separator_tool_item_new();
    gtk_toolbar_insert(GTK_TOOLBAR(toolbar),toolbar_sep,-1);
    
    toolbar_read = gtk_tool_button_new_from_stock(GTK_STOCK_GO_DOWN);
    gtk_toolbar_insert(GTK_TOOLBAR(toolbar),toolbar_read,-1);
    gtk_tool_item_set_tooltip_text(toolbar_read,"Retrieve nxyters config");
    
    toolbar_write = gtk_tool_button_new_from_stock(GTK_STOCK_GO_UP);
    gtk_toolbar_insert(GTK_TOOLBAR(toolbar),toolbar_write,-1);
    gtk_tool_item_set_tooltip_text(toolbar_write,"Write config to nxyters");

    toolbar_sep = gtk_separator_tool_item_new();
    gtk_toolbar_insert(GTK_TOOLBAR(toolbar),toolbar_sep,-1);

    toolbar_quit = gtk_tool_button_new_from_stock(GTK_STOCK_QUIT);
    gtk_toolbar_insert(GTK_TOOLBAR(toolbar),toolbar_quit,-1);
    gtk_tool_item_set_tooltip_text(toolbar_quit,"Quit");


    gtk_box_pack_start(GTK_BOX(vbox),toolbar,FALSE,FALSE,5);


    //2nd row in vbox
    hbox = gtk_hbox_new(FALSE,0);
    gtk_box_pack_start(GTK_BOX(vbox),hbox,FALSE,FALSE,5);



    /// Nxyter selector widget, to choose nxyter at specified sfp, exploder
    frame1 = gtk_frame_new("SFPs setting");
    gtk_box_pack_start(GTK_BOX(hbox),frame1,FALSE,FALSE,5);

    label_sfpconfig = gtk_label_new(NULL);
    gtk_container_add(GTK_CONTAINER(frame1),label_sfpconfig);
    set_sfpconfig_label(label_sfpconfig);

    frame3 = gtk_frame_new("nxy selector");
    gtk_box_pack_start(GTK_BOX(hbox),frame3,FALSE,FALSE,5);

    table_nxyselector = gtk_table_new(4,2,TRUE);
    gtk_container_add(GTK_CONTAINER(frame3),table_nxyselector);

    sfp_widget = gtk_spin_button_new_with_range(0,0,1);
    exp_widget = gtk_spin_button_new_with_range(0,0,1);
    nxysel_widget = gtk_spin_button_new_with_range(0,1,1);
    nxycounter_widget = gtk_spin_button_new_with_range(0,0,1);
    
    label = gtk_label_new("SFP");
    gtk_table_attach_defaults(GTK_TABLE(table_nxyselector),label,0,1,0,1);
    label = gtk_label_new("EXP");
    gtk_table_attach_defaults(GTK_TABLE(table_nxyselector),label,0,1,1,2);
    label = gtk_label_new("NXY");
    gtk_table_attach_defaults(GTK_TABLE(table_nxyselector),label,0,1,2,3); 
    label = gtk_label_new("NXY#");
    gtk_table_attach_defaults(GTK_TABLE(table_nxyselector),label,0,1,3,4);
   
    gtk_table_attach_defaults(GTK_TABLE(table_nxyselector),sfp_widget,1,2,0,1);
    gtk_table_attach_defaults(GTK_TABLE(table_nxyselector),exp_widget,1,2,1,2);
    gtk_table_attach_defaults(GTK_TABLE(table_nxyselector),nxysel_widget,1,2,2,3);
    gtk_table_attach_defaults(GTK_TABLE(table_nxyselector),nxycounter_widget,1,2,3,4);
    
    frame3 = gtk_frame_new("nxy PC name: ");
    gtk_box_pack_start(GTK_BOX(hbox),frame3,FALSE,FALSE,5);
    socketname_widget = gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(socketname_widget),DEFAULT_SOCKNAME);
    gtk_container_add(GTK_CONTAINER(frame3),socketname_widget);
    
    
    /// global registers widget
    frame3 = gtk_frame_new("Global registers setting: ");
    gtk_box_pack_start(GTK_BOX(hbox),frame3,FALSE,FALSE,5);
    
    pre_trg_widget = gtk_spin_button_new_with_range(0,255,1);
    post_trg_widget = gtk_spin_button_new_with_range(0,255,1);
    test_pulse_delay_widget = gtk_spin_button_new_with_range(0,255,1);
    test_trg_delay_widget = gtk_spin_button_new_with_range(0,255,1);
    exploder_mode_widget = gtk_toggle_button_new();
    
    table_nxyselector = gtk_table_new(5,2,FALSE);
    gtk_container_add(GTK_CONTAINER(frame3),table_nxyselector);
    
    label = gtk_label_new("Pre window");
    gtk_table_attach_defaults(GTK_TABLE(table_nxyselector),label,0,1,0,1);
    tooltips = gtk_tooltips_new();
    gtk_tooltips_set_tip(GTK_TOOLTIPS(tooltips),label,"Pre window and Post window must be both even or both odd!",NULL);
    label = gtk_label_new("Post window");
    gtk_table_attach_defaults(GTK_TABLE(table_nxyselector),label,0,1,1,2);
    tooltips = gtk_tooltips_new();
    gtk_tooltips_set_tip(GTK_TOOLTIPS(tooltips),label,"Pre window and Post window must be both even or both odd!",NULL);
    label = gtk_label_new("Test pulse delay");
    gtk_table_attach_defaults(GTK_TABLE(table_nxyselector),label,0,1,2,3);
    label = gtk_label_new("Test trigger delay");
    gtk_table_attach_defaults(GTK_TABLE(table_nxyselector),label,0,1,3,4);
    
    gtk_table_attach_defaults(GTK_TABLE(table_nxyselector),pre_trg_widget,1,2,0,1);
    gtk_table_attach_defaults(GTK_TABLE(table_nxyselector),post_trg_widget,1,2,1,2);
    gtk_table_attach_defaults(GTK_TABLE(table_nxyselector),test_pulse_delay_widget,1,2,2,3);
    gtk_table_attach_defaults(GTK_TABLE(table_nxyselector),test_trg_delay_widget,1,2,3,4);
    gtk_table_attach_defaults(GTK_TABLE(table_nxyselector),exploder_mode_widget,0,2,4,5);
    
    
    /// nxy register viewer widget, to show registers of selected nxyter
    frame3 = gtk_frame_new("Nxyter: ");
    gtk_box_pack_start(GTK_BOX(vbox),frame3,FALSE,FALSE,5);
    
    nxyreg_widget = nxywidget_new();
    gtk_container_add(GTK_CONTAINER(frame3),nxyreg_widget);        
    
    /// shortcuts                    
    sc_input = gtk_entry_new();
    sc_vth = gtk_button_new_with_label("Set Vths");
    tooltips = gtk_tooltips_new();
	gtk_tooltips_set_tip(GTK_TOOLTIPS(tooltips),sc_vth,"Set value (hex or dec) Vth value to all nxyters",NULL);
	
	hbox = gtk_hbox_new(FALSE,0);
    gtk_box_pack_start(GTK_BOX(vbox),hbox,FALSE,FALSE,5);
    
	frame3 = gtk_frame_new("Shortcuts: ");    
    gtk_box_pack_start(GTK_BOX(hbox),frame3,FALSE,FALSE,5);
    table = gtk_table_new(5,2,FALSE);
    gtk_container_add(GTK_CONTAINER(frame3),table);
    gtk_table_attach_defaults(GTK_TABLE(table),sc_input,0,1,0,1);
    gtk_table_attach_defaults(GTK_TABLE(table),sc_vth,0,1,1,2);
    
    		
	
    

	////////////////////////// signals //////////////////////////////////////
    g_signal_connect (window, "destroy",G_CALLBACK (destroy), NULL);

    g_signal_connect(G_OBJECT(toolbar_quit),"clicked",G_CALLBACK(gtk_main_quit),NULL);
    g_signal_connect(G_OBJECT(toolbar_open),"clicked",G_CALLBACK(open_file),window);
    g_signal_connect(G_OBJECT(toolbar_save),"clicked",G_CALLBACK(save_file),window);
    g_signal_connect(G_OBJECT(toolbar_read),"clicked",G_CALLBACK(read_function),window);
    g_signal_connect(G_OBJECT(toolbar_write),"clicked",G_CALLBACK(write_function),window);
    
    g_signal_connect(G_OBJECT(nxysel_widget),"value-changed",G_CALLBACK(change_current_nxy),&yes);
    g_signal_connect(G_OBJECT(exp_widget),"value-changed",G_CALLBACK(change_current_nxy),&yes);
    g_signal_connect(G_OBJECT(sfp_widget),"value-changed",G_CALLBACK(change_current_nxy),&yes);
    g_signal_connect(G_OBJECT(nxycounter_widget),"value-changed",G_CALLBACK(change_current_nxy),&no);
    g_signal_connect(G_OBJECT(exploder_mode_widget),"clicked",G_CALLBACK(change_global_widgets),&no);
    
    g_signal_connect(G_OBJECT(sc_vth),"clicked",G_CALLBACK(sc_vth_clicked),&no);
   
    g_signal_connect_swapped(G_OBJECT(window),"destroy",G_CALLBACK(gtk_main_quit),NULL);

    gtk_widget_show_all(window);
    gtk_main ();
    g_string_free(global_filename,TRUE);
    free(reg);
    printf("the end\n");
    return 0;
}
