/**
 * @file
 * A.Prochazka
 * 13.7.2013
 * Source file for custom GTK widget to show registers of nxyter
 */ 
#include "nxywidget.h"
#include <string.h>
#include <stdlib.h>

enum{
	NXY_WIDGET_SIGNAL,
	LAST_SIGNAL
	};

/**
 * Funtion where all initialization takes place
 * all subwidgets are initialized here
 */ 
static void nxywidget_init(NxyWidget *nxywidget);
static void nxywidget_class_init(NxyWidgetClass *klass);

/**
 * This function is called when config register value is changed
 * using direclty the register value.
 * This also adjust toggle buttons according to new value
 */  
static void _nxywidget_config_changed( GtkWidget *widget, gpointer   data );

/**
 * This function is called when configuration register buttons are clicked
 * It than calculates new registers values.
 */ 
static void _nxywidget_config_buttons_clicked( GtkWidget *widget, gpointer   data );

static void _nxywidget_mask_button_clicked( GtkWidget *widget, gpointer data );
static void _nxywidget_unmask_button_clicked( GtkWidget *widget, gpointer data );
static void _nxywidget_maskreg_button_clicked( GtkWidget *widget, gpointer data );

/**
 * Function which is called when NxyWidget need to be updated.
 */ 
static void _nxywidget_update(NxyWidget *n);

/**
 * This function is called when local thr. channel selector is changed.
 */ 
static void _nxywidget_th_channel_changed(GtkWidget *widget, gpointer data);
/**
 * This function is called when local thr. value selector is changed.
 */ 
static void _nxywidget_th_value_changed(GtkWidget *widget, gpointer data);
/**
 * This function is called when local thr. apply all button is clicked
 */ 
static void _nxywidget_th_value_changed_all(GtkWidget *widget, gpointer data);

static guint nxy_widget_signals[LAST_SIGNAL] = {0};

/// Function Definitions ///	
	
GType nxywidget_get_type(void)	
{
	static GType entry_type = 0;
	if(!entry_type)
	{
		static const GTypeInfo entry_info = {
			sizeof(NxyWidgetClass),
			NULL,
			NULL,
			(GClassInitFunc) nxywidget_class_init,
			NULL,
			NULL,
			sizeof(NxyWidget),
			0,
			(GInstanceInitFunc) nxywidget_init
			};
		entry_type = g_type_register_static(GTK_TYPE_VBOX,"NxyWidget",&entry_info,0);
		}
	return entry_type;
}

static void nxywidget_class_init(NxyWidgetClass *klass){
	nxy_widget_signals[NXY_WIDGET_SIGNAL] = g_signal_new("nxy-changed",G_TYPE_FROM_CLASS(klass),G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION, G_STRUCT_OFFSET(NxyWidgetClass,nxywidget),NULL,NULL,g_cclosure_marshal_VOID__VOID,G_TYPE_NONE,0);
}

static void nxywidget_init(NxyWidget *n){
	GtkWidget *label;
	
	GtkWidget *biaspage;
	GtkWidget *configpage;
	GtkWidget *delaypage;
	GtkWidget *thpage;
	GtkWidget *maskpage;
	
	GtkWidget *box1;
	GtkWidget *box2;
	GtkWidget *box3;
	GtkWidget *box4;

	GtkTooltips *tooltips;
	
	int i,j;
	
	n->index = -1;	
	n->_r = NULL;

	n->nb = gtk_notebook_new();
	
	//bias
	biaspage = gtk_table_new(7,4,FALSE);	
	
	i=0;
	label = gtk_label_new("Icg ");
	gtk_table_attach_defaults(GTK_TABLE(biaspage), label, 0,1,0,1);
	n->bias[i] = gtk_spin_button_new_with_range(0,255,1);
	gtk_table_attach_defaults(GTK_TABLE(biaspage), n->bias[i], 1,2,0,1);
	tooltips = gtk_tooltips_new();
	gtk_tooltips_set_tip(GTK_TOOLTIPS(tooltips),label,"This determines the gate voltage of the preamplifiers current sink",NULL);
	
	i=1;
	label = gtk_label_new("Icgfoll ");
	gtk_table_attach_defaults(GTK_TABLE(biaspage), label, 0,1,1,2);	
	n->bias[i] = gtk_spin_button_new_with_range(0,255,1);
	gtk_table_attach_defaults(GTK_TABLE(biaspage), n->bias[i], 1,2,1,2);
	tooltips = gtk_tooltips_new();
	gtk_tooltips_set_tip(GTK_TOOLTIPS(tooltips),label,"This sets the bias voltage for the shaper and both stages of the slow shaper",NULL);

	
	i=2;
	label = gtk_label_new("Vth ");
	gtk_table_attach_defaults(GTK_TABLE(biaspage), label, 0,1,2,3);
	n->bias[i] = gtk_spin_button_new_with_range(0,255,1);
	gtk_table_attach_defaults(GTK_TABLE(biaspage), n->bias[i], 1,2,2,3);
	tooltips = gtk_tooltips_new();
	gtk_tooltips_set_tip(GTK_TOOLTIPS(tooltips),label,"Global threshold voltage for the comparators",NULL);

		
	i=3;
	label = gtk_label_new("Vbfb ");
	gtk_table_attach_defaults(GTK_TABLE(biaspage), label, 0,1,3,4);
	n->bias[i] = gtk_spin_button_new_with_range(0,255,1);
	gtk_table_attach_defaults(GTK_TABLE(biaspage), n->bias[i], 1,2,3,4);
	tooltips = gtk_tooltips_new();
	gtk_tooltips_set_tip(GTK_TOOLTIPS(tooltips),label,"This sets the discharge time for the preamplifier. It is also upper limit for the output voltage.",NULL);
	

	i=4;
	label = gtk_label_new("VbiasF ");
	gtk_table_attach_defaults(GTK_TABLE(biaspage), label, 0,1,4,5);
	n->bias[i] = gtk_spin_button_new_with_range(0,255,1);
	gtk_table_attach_defaults(GTK_TABLE(biaspage), n->bias[i], 1,2,4,5);
	tooltips = gtk_tooltips_new();
	gtk_tooltips_set_tip(GTK_TOOLTIPS(tooltips),label,"Bias voltage for the fast shaper",NULL);
	

	i=5;
	label = gtk_label_new("VbiasS ");	
	gtk_table_attach_defaults(GTK_TABLE(biaspage), label, 0,1,5,6);
	n->bias[i] = gtk_spin_button_new_with_range(0,255,1);
	gtk_table_attach_defaults(GTK_TABLE(biaspage), n->bias[i], 1,2,5,6);
	tooltips = gtk_tooltips_new();
	gtk_tooltips_set_tip(GTK_TOOLTIPS(tooltips),label,"Bias voltage of the first stage of the slow shaper",NULL);
	


	i=6;
	label = gtk_label_new("VbiasS2 ");
	gtk_table_attach_defaults(GTK_TABLE(biaspage), label, 0,1,6,7);
	n->bias[i] = gtk_spin_button_new_with_range(0,255,1);
	gtk_table_attach_defaults(GTK_TABLE(biaspage), n->bias[i], 1,2,6,7);
	tooltips = gtk_tooltips_new();
	gtk_tooltips_set_tip(GTK_TOOLTIPS(tooltips),label,"Bias voltage of the seconf stage of the slow shaper",NULL);
	
	

	i=7;
	label = gtk_label_new("Vcm ");
	gtk_table_attach_defaults(GTK_TABLE(biaspage), label, 3,4,0,1);
	n->bias[i] = gtk_spin_button_new_with_range(0,255,1);
	gtk_table_attach_defaults(GTK_TABLE(biaspage), n->bias[i], 4,5,0,1);
	tooltips = gtk_tooltips_new();
	gtk_tooltips_set_tip(GTK_TOOLTIPS(tooltips),label,"This adjusts the target voltage of the common mode feedback that stabilizes the output",NULL);

	
	i=8;
	label = gtk_label_new("cal ");
	gtk_table_attach_defaults(GTK_TABLE(biaspage), label, 3,4,1,2);
	n->bias[i] = gtk_spin_button_new_with_range(0,255,1);
	gtk_table_attach_defaults(GTK_TABLE(biaspage), n->bias[i], 4,5,1,2);
	tooltips = gtk_tooltips_new();
	gtk_tooltips_set_tip(GTK_TOOLTIPS(tooltips),label,"Test pulse strength",NULL);
	

	i=9;
	label = gtk_label_new("iComp ");
	gtk_table_attach_defaults(GTK_TABLE(biaspage), label, 3,4,2,3);
	n->bias[i] = gtk_spin_button_new_with_range(0,255,1);
	gtk_table_attach_defaults(GTK_TABLE(biaspage), n->bias[i], 4,5,2,3);
	tooltips = gtk_tooltips_new();
	gtk_tooltips_set_tip(GTK_TOOLTIPS(tooltips),label,"Current for the main stage of the comparator",NULL);

	
	i=10;
	label = gtk_label_new("iDur ");
	gtk_table_attach_defaults(GTK_TABLE(biaspage), label, 3,4,3,4);
	n->bias[i] = gtk_spin_button_new_with_range(0,255,1);
	gtk_table_attach_defaults(GTK_TABLE(biaspage), n->bias[i], 4,5,3,4);
	tooltips = gtk_tooltips_new();
	gtk_tooltips_set_tip(GTK_TOOLTIPS(tooltips),label,"This adjusts the dead-time of the analog channels",NULL);


	
	i=11;
	label = gtk_label_new("iINV ");
	gtk_table_attach_defaults(GTK_TABLE(biaspage), label, 3,4,4,5);
	n->bias[i] = gtk_spin_button_new_with_range(0,255,1);
	gtk_table_attach_defaults(GTK_TABLE(biaspage), n->bias[i], 4,5,4,5);
	tooltips = gtk_tooltips_new();
	gtk_tooltips_set_tip(GTK_TOOLTIPS(tooltips),label,"Current in the output stage of the comparator",NULL);

	
	i=12;
	label = gtk_label_new("iPDH ");
	gtk_table_attach_defaults(GTK_TABLE(biaspage), label, 3,4,5,6);
	n->bias[i] = gtk_spin_button_new_with_range(0,255,1);
	gtk_table_attach_defaults(GTK_TABLE(biaspage), n->bias[i], 4,5,5,6);
	tooltips = gtk_tooltips_new();
	gtk_tooltips_set_tip(GTK_TOOLTIPS(tooltips),label,"Current in the peak detect and hold citcuit",NULL);


	i=13;
	label = gtk_label_new("iTWC ");
	gtk_table_attach_defaults(GTK_TABLE(biaspage), label, 3,4,6,7);
	n->bias[i] = gtk_spin_button_new_with_range(0,255,1);
	gtk_table_attach_defaults(GTK_TABLE(biaspage), n->bias[i], 4,5,6,7);	
	tooltips = gtk_tooltips_new();
	gtk_tooltips_set_tip(GTK_TOOLTIPS(tooltips),label,"Current in the time walk compensation cicuit",NULL);

	
	
	//config	
	configpage = gtk_table_new(5,9,FALSE);

	label = gtk_label_new("#32 ");
	gtk_table_attach_defaults(GTK_TABLE(configpage), label, 0,1,0,1);
	n->config[0] = gtk_spin_button_new_with_range(0,255,1);
	gtk_table_attach_defaults(GTK_TABLE(configpage), n->config[0], 1,2,0,1);
	
	label = gtk_label_new("#33 ");
	gtk_table_attach_defaults(GTK_TABLE(configpage), label, 3,4,0,1);
	n->config[1] = gtk_spin_button_new_with_range(0,15,1);
	gtk_table_attach_defaults(GTK_TABLE(configpage), n->config[1], 4,5,0,1);
	
	n->c32_0 = gtk_toggle_button_new();
	label = gtk_label_new("Test pulse: ");
	gtk_table_attach_defaults(GTK_TABLE(configpage), label, 0,1,1,2);
	gtk_table_attach_defaults(GTK_TABLE(configpage), n->c32_0, 1,2,1,2);
	tooltips = gtk_tooltips_new();
	gtk_tooltips_set_tip(GTK_TOOLTIPS(tooltips),label,"This enables the test pulse signal for the calibration block",NULL);
	
	n->c32_1 = gtk_toggle_button_new();
	label = gtk_label_new("Test pulse sync: ");
	gtk_table_attach_defaults(GTK_TABLE(configpage), label, 0,1,2,3);
	gtk_table_attach_defaults(GTK_TABLE(configpage), n->c32_1, 1,2,2,3);
	tooltips = gtk_tooltips_new();
	gtk_tooltips_set_tip(GTK_TOOLTIPS(tooltips),label,"Test pulse synchronisation with clk256A clock",NULL);
	
	n->c32_2 = gtk_toggle_button_new();
	label = gtk_label_new("Test pulse polarity: ");
	gtk_table_attach_defaults(GTK_TABLE(configpage), label, 0,1,3,4);
	gtk_table_attach_defaults(GTK_TABLE(configpage), n->c32_2, 1,2,3,4);
	tooltips = gtk_tooltips_new();
	gtk_tooltips_set_tip(GTK_TOOLTIPS(tooltips),label,"Test pulse polarity",NULL);
	
	n->c32_3 = gtk_toggle_button_new();
	label = gtk_label_new("Test Trigger: ");
	gtk_table_attach_defaults(GTK_TABLE(configpage), label, 0,1,4,5);
	gtk_table_attach_defaults(GTK_TABLE(configpage), n->c32_3, 1,2,4,5);
	tooltips = gtk_tooltips_new();
	gtk_tooltips_set_tip(GTK_TOOLTIPS(tooltips),label,"Test trigger signals",NULL);
	
	n->c32_4 = gtk_toggle_button_new();
	label = gtk_label_new("Test Trigger Sync: ");
	gtk_table_attach_defaults(GTK_TABLE(configpage), label, 0,1,5,6);
	gtk_table_attach_defaults(GTK_TABLE(configpage), n->c32_4, 1,2,5,6);
	tooltips = gtk_tooltips_new();
	gtk_tooltips_set_tip(GTK_TOOLTIPS(tooltips),label,"Test trigger synchronisation with clk256A clock",NULL);
	
	n->c32_5 = gtk_toggle_button_new();
	label = gtk_label_new("32Mhz clock: ");
	gtk_table_attach_defaults(GTK_TABLE(configpage), label, 0,1,6,7);
	gtk_table_attach_defaults(GTK_TABLE(configpage), n->c32_5, 1,2,6,7);
	
	n->c32_6 = gtk_toggle_button_new();
	label = gtk_label_new("128Mhz clock: ");
	gtk_table_attach_defaults(GTK_TABLE(configpage), label, 0,1,7,8);
	gtk_table_attach_defaults(GTK_TABLE(configpage), n->c32_6, 1,2,7,8);
	
	n->c32_7 = gtk_toggle_button_new();
	label = gtk_label_new("LSB clock: ");
	gtk_table_attach_defaults(GTK_TABLE(configpage), label, 0,1,8,9);
	gtk_table_attach_defaults(GTK_TABLE(configpage), n->c32_7, 1,2,8,9);
	tooltips = gtk_tooltips_new();
	gtk_tooltips_set_tip(GTK_TOOLTIPS(tooltips),label,"This selects the clock from which LSB of the timestamp is derivered.",NULL);


	n->c33_0 = gtk_toggle_button_new();
	label = gtk_label_new("Cal.sel. 1st bit: ");
	gtk_table_attach_defaults(GTK_TABLE(configpage), label, 3,4,1,2);
	gtk_table_attach_defaults(GTK_TABLE(configpage), n->c33_0, 4,5,1,2);
	tooltips = gtk_tooltips_new();
	gtk_tooltips_set_tip(GTK_TOOLTIPS(tooltips),label,"First 2 bits of register #33 selects the set of channels to which the calibration pulse is applied:\n 00 - channels # with modulo 0\n 01 - channels # with modulo 1\n 10 - channels # with modulo 2\n 11 - channels # with modulo 3",NULL);

	
	n->c33_1 = gtk_toggle_button_new();
	label = gtk_label_new("Cal.sel. 2nd bit: ");
	gtk_table_attach_defaults(GTK_TABLE(configpage), label, 3,4,2,3);
	gtk_table_attach_defaults(GTK_TABLE(configpage), n->c33_1, 4,5,2,3);
	tooltips = gtk_tooltips_new();
	gtk_tooltips_set_tip(GTK_TOOLTIPS(tooltips),label,"First 2 bits of register #33 selects the set of channels to which the calibration pulse is applied:\n 00 - channels # with modulo 0\n 01 - channels # with modulo 1\n 10 - channels # with modulo 2\n 11 - channels # with modulo 3",NULL);
	
	n->c33_2 = gtk_toggle_button_new();
	label = gtk_label_new("Polarity: ");
	gtk_table_attach_defaults(GTK_TABLE(configpage), label, 3,4,3,4);
	gtk_table_attach_defaults(GTK_TABLE(configpage), n->c33_2, 4,5,3,4);
	tooltips = gtk_tooltips_new();
	gtk_tooltips_set_tip(GTK_TOOLTIPS(tooltips),label,"Front-end polarity",NULL);

	
	n->c33_3 = gtk_toggle_button_new();
	label = gtk_label_new("Readout clock: ");
	gtk_table_attach_defaults(GTK_TABLE(configpage), label, 3,4,4,5);
	gtk_table_attach_defaults(GTK_TABLE(configpage), n->c33_3, 4,5,4,5);
	tooltips = gtk_tooltips_new();
	gtk_tooltips_set_tip(GTK_TOOLTIPS(tooltips),label,"This selects the clock from which 32Mhz readout clock is derivered. \nWhen clk256A is selected: clk128 is clk256A divided by 2 and clk32 is clk256 divided by 8.\nWhen clk256B is selected clk128 is buffered clk256B and clk32 is clk256B divided by 4 ",NULL);
	
		
	
	//clock delay
	delaypage = gtk_vbox_new(0,1);
	box1 = gtk_hbox_new(0,1);	
	box2 = gtk_hbox_new(0,1);
	label = gtk_label_new("Clock delays: ");
	gtk_box_pack_start(GTK_BOX(box1),label,0,0,2);
	
	
	for(i=0;i<3;i++)
	{
		n->clk_del[i] = gtk_spin_button_new_with_range(0,255,1);
		gtk_box_pack_start(GTK_BOX(box1),n->clk_del[i],0,0,2);
		}
	//test delay
	label = gtk_label_new("Test delays: ");
	gtk_box_pack_start(GTK_BOX(box2),label,0,0,2);
	for(i=0;i<2;i++)
	{
		n->te_del[i] = gtk_spin_button_new_with_range(0,255,1);
		gtk_box_pack_start(GTK_BOX(box2),n->te_del[i],0,0,2);
		}
	gtk_box_pack_start(GTK_BOX(delaypage),box1,0,0,20);
	gtk_box_pack_start(GTK_BOX(delaypage),box2,0,0,2);
		
	//thresholds

	//thpage = gtk_table_new(4,3,FALSE);
	thpage = gtk_hbox_new(0,1);
	box1 = gtk_hbox_new(0,1);
	box2 = gtk_hbox_new(0,1);
	box3 = gtk_vbox_new(0,1);
	box4 = gtk_frame_new("local thresholds");
	
	label = gtk_label_new("Channel: ");	
	gtk_box_pack_start(GTK_BOX(box1),label,0,0,2);
	n->th_channel = gtk_spin_button_new_with_range(0,127,1);
	gtk_box_pack_start(GTK_BOX(box1),n->th_channel,0,0,2);	
	
	label = gtk_label_new("Th value: ");	
	gtk_box_pack_start(GTK_BOX(box2),label,0,0,2);
	n->th_value = gtk_spin_button_new_with_range(0,32,1);
	gtk_box_pack_start(GTK_BOX(box2),n->th_value,0,0,2);

	n->th_all_button = gtk_button_new_with_label("Apply to all");
	

	gtk_box_pack_start(GTK_BOX(box3),box1,0,0,2);
	gtk_box_pack_start(GTK_BOX(box3),box2,0,0,2);
	gtk_box_pack_start(GTK_BOX(box3),n->th_all_button,0,0,2);
	
	gtk_container_add(GTK_CONTAINER(box4),box3);
	
	box3 = gtk_vbox_new(0,1);
	label = gtk_label_new("Test th: ");	
	gtk_box_pack_start(GTK_BOX(box3),label,0,0,2);
	n->th_te = gtk_spin_button_new_with_range(0,255,1);
	gtk_box_pack_start(GTK_BOX(box3),n->th_te,0,0,2);

	
	gtk_box_pack_start(GTK_BOX(thpage),box4,0,0,5);
	gtk_box_pack_start(GTK_BOX(thpage),box3,0,0,5);
	

	
	//masks
	maskpage = gtk_vbox_new(FALSE,1);	
		
	box4 = gtk_frame_new("Masked Channels");
	n->masked_channels = gtk_label_new("N/A");
	gtk_label_set_line_wrap(GTK_LABEL(n->masked_channels),1);
	gtk_container_add(GTK_CONTAINER(box4),n->masked_channels);

	box2 = gtk_hbox_new(FALSE,1);
	box3 = gtk_hbox_new(FALSE,1);
	
	n->channel_input = gtk_entry_new();
	n->mask_button = gtk_button_new_with_label("Mask");
	n->unmask_button = gtk_button_new_with_label("Unmask");
	n->maskreg_button = gtk_button_new_with_label("Set Masks");
	tooltips = gtk_tooltips_new();
	gtk_tooltips_set_tip(GTK_TOOLTIPS(tooltips),n->maskreg_button,"Set value (hex or dec) to all mask registers",NULL);
	
	gtk_box_pack_start(GTK_BOX(box2),n->channel_input,FALSE,TRUE,0);
	gtk_box_pack_start(GTK_BOX(box3),n->mask_button,FALSE,FALSE,1);
	gtk_box_pack_start(GTK_BOX(box3),n->unmask_button,FALSE,FALSE,1);
	gtk_box_pack_start(GTK_BOX(box3),n->maskreg_button,FALSE,FALSE,1);

	gtk_box_pack_start(GTK_BOX(maskpage),box4,FALSE,FALSE,10);
	gtk_box_pack_start(GTK_BOX(maskpage),box2,FALSE,FALSE,2);
	gtk_box_pack_start(GTK_BOX(maskpage),box3,FALSE,FALSE,2);

	
	label = gtk_label_new("Bias");
	gtk_notebook_append_page(GTK_NOTEBOOK(n->nb),biaspage,label);	
	
	
	label = gtk_label_new("Config");
	gtk_notebook_append_page(GTK_NOTEBOOK(n->nb),configpage,label);	
	
	label = gtk_label_new("Delays");
	gtk_notebook_append_page(GTK_NOTEBOOK(n->nb),delaypage,label);	
	
	label = gtk_label_new("Masks");
	gtk_notebook_append_page(GTK_NOTEBOOK(n->nb),maskpage,label);	
	
	label = gtk_label_new("Thresholds");
	gtk_notebook_append_page(GTK_NOTEBOOK(n->nb),thpage,label);	
	
	gtk_container_add(GTK_CONTAINER(n), n->nb);


	g_signal_connect(G_OBJECT(n->config[0]),"value-changed",G_CALLBACK(_nxywidget_config_changed),n);
	g_signal_connect(G_OBJECT(n->config[1]),"value-changed",G_CALLBACK(_nxywidget_config_changed),n);
	g_signal_connect(G_OBJECT(n->mask_button),"clicked",G_CALLBACK(_nxywidget_mask_button_clicked),n);
	g_signal_connect(G_OBJECT(n->unmask_button),"clicked",G_CALLBACK(_nxywidget_unmask_button_clicked),n);
	g_signal_connect(G_OBJECT(n->maskreg_button),"clicked",G_CALLBACK(_nxywidget_maskreg_button_clicked),n);
	g_signal_connect(G_OBJECT(n->th_channel),"value-changed",G_CALLBACK(_nxywidget_th_channel_changed),n);
	g_signal_connect(G_OBJECT(n->th_value),"value-changed",G_CALLBACK(_nxywidget_th_value_changed),n);
	g_signal_connect(G_OBJECT(n->th_te),"value-changed",G_CALLBACK(_nxywidget_th_value_changed),n);
	g_signal_connect(G_OBJECT(n->th_all_button),"clicked",G_CALLBACK(_nxywidget_th_value_changed_all),n);
	
	
	g_signal_connect(G_OBJECT(n->c32_0),"clicked",G_CALLBACK(_nxywidget_config_buttons_clicked),n);
	g_signal_connect(G_OBJECT(n->c32_1),"clicked",G_CALLBACK(_nxywidget_config_buttons_clicked),n);
	g_signal_connect(G_OBJECT(n->c32_2),"clicked",G_CALLBACK(_nxywidget_config_buttons_clicked),n);
	g_signal_connect(G_OBJECT(n->c32_3),"clicked",G_CALLBACK(_nxywidget_config_buttons_clicked),n);
	g_signal_connect(G_OBJECT(n->c32_4),"clicked",G_CALLBACK(_nxywidget_config_buttons_clicked),n);
	g_signal_connect(G_OBJECT(n->c32_5),"clicked",G_CALLBACK(_nxywidget_config_buttons_clicked),n);
	g_signal_connect(G_OBJECT(n->c32_6),"clicked",G_CALLBACK(_nxywidget_config_buttons_clicked),n);	
	g_signal_connect(G_OBJECT(n->c32_7),"clicked",G_CALLBACK(_nxywidget_config_buttons_clicked),n);

	g_signal_connect(G_OBJECT(n->c33_0),"clicked",G_CALLBACK(_nxywidget_config_buttons_clicked),n);
	g_signal_connect(G_OBJECT(n->c33_1),"clicked",G_CALLBACK(_nxywidget_config_buttons_clicked),n);
	g_signal_connect(G_OBJECT(n->c33_2),"clicked",G_CALLBACK(_nxywidget_config_buttons_clicked),n);
	g_signal_connect(G_OBJECT(n->c33_3),"clicked",G_CALLBACK(_nxywidget_config_buttons_clicked),n);

}


GtkWidget * nxywidget_new()
{
	return GTK_WIDGET(g_object_new(nxywidget_get_type(),NULL));
	}

void nxywidget_set(NxyWidget *n, struct nxyreg *r, int index){
	int i,j;
	GString *txtmasked = g_string_sized_new(2);
	
	if(index<0)return;
	n->index = index;
	n->_r = r;
	//bias
	for(i=0;i<14;i++){
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(n->bias[i]),r[index].bias[i]);
		}
	
	//masks
	for(i=0;i<16;i++){
		//gtk_spin_button_set_value(GTK_SPIN_BUTTON(n->mask[i]),r[index].mask[i]);
		if(r[index].mask[i]!=0){
			for(j=0;j<8;j++){
				if(r[index].mask[i]&(1<<j))
					g_string_append_printf(txtmasked,"%d, ",((i*8)+(j)));
				}
			}
		}	
	gtk_label_set_label(GTK_LABEL(n->masked_channels),txtmasked->str);		

	//thresholds
	_nxywidget_th_channel_changed(NULL,n);
	
	//config
	for(i=0;i<2;i++){
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(n->config[i]),r[index].config[i]);
		}
	_nxywidget_config_changed(NULL,n);
		
	//test delay
	for(i=0;i<2;i++){
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(n->te_del[i]),r[index].te_del[i]);
		}
		
	for(i=0;i<3;i++){
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(n->clk_del[i]),r[index].clk_del[i]);
		}	
		
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(n->th_te),r[index].thr_te);
			
	g_string_free(txtmasked,1);
	}

static void _nxywidget_update(NxyWidget *n){
	int i,j;
	GString *txtmasked = g_string_sized_new(2);
	
	//bias
	for(i=0;i<14;i++){
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(n->bias[i]),n->_r[n->index].bias[i]);
		}
	
	//masks
	for(i=0;i<16;i++){
		//gtk_spin_button_set_value(GTK_SPIN_BUTTON(n->mask[i]),r[index].mask[i]);
		if(n->_r[n->index].mask[i]!=0){
			for(j=0;j<8;j++){
				if(n->_r[n->index].mask[i]&(1<<j))
					g_string_append_printf(txtmasked,"%d, ",((i*8)+(j)));
				}
			}
		}	
	gtk_label_set_label(GTK_LABEL(n->masked_channels),txtmasked->str);		

	//thresholds
	_nxywidget_th_channel_changed(NULL,n);
	
	//config
	for(i=0;i<2;i++){
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(n->config[i]),n->_r[n->index].config[i]);
		}
	_nxywidget_config_changed(NULL,n);
		
	//test delay
	for(i=0;i<2;i++){
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(n->te_del[i]),n->_r[n->index].te_del[i]);
		}
		
	for(i=0;i<3;i++){
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(n->clk_del[i]),n->_r[n->index].clk_del[i]);
		}	
		
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(n->th_te),n->_r[n->index].thr_te);
			
	g_string_free(txtmasked,1);	
	}
	
int nxywidget_get(NxyWidget *n, struct nxyreg *r){
	int i;
	if(n->index < 0)return 0;	
	//bias
	for(i=0;i<14;i++){
		gtk_spin_button_update(GTK_SPIN_BUTTON(n->bias[i]));
		r[n->index].bias[i] = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(n->bias[i]));
		}	
	
	//config
	for(i=0;i<2;i++){
		gtk_spin_button_update(GTK_SPIN_BUTTON(n->config[i]));
		r[n->index].config[i] = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(n->config[i]));
		}
		
	//test delay
	for(i=0;i<2;i++){
		gtk_spin_button_update(GTK_SPIN_BUTTON(n->te_del[i]));
		r[n->index].te_del[i] = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(n->te_del[i]));
		}
		
	for(i=0;i<3;i++){
		gtk_spin_button_update(GTK_SPIN_BUTTON(n->clk_del[i]));
		r[n->index].clk_del[i] = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(n->clk_del[i]));
		}	
		
	gtk_spin_button_update(GTK_SPIN_BUTTON(n->th_te));	
	r[n->index].thr_te = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(n->th_te));
			
	return 1;		
	}	

int nxywidget_get_index(NxyWidget *n){
	return n->index;
	}
	
static void _nxywidget_config_changed( GtkWidget *widget, gpointer data ){
	int c1,c2;
	NxyWidget *_n = data;
	
	if(_n->_r==NULL)return;
	
	c1 = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(_n->config[0]));
	c2 = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(_n->config[1]));

	if(c1&1){
		gtk_button_set_label(GTK_BUTTON(_n->c32_0),"enabled");
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(_n->c32_0),1);
		}
	else{
		gtk_button_set_label(GTK_BUTTON(_n->c32_0),"disabled");
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(_n->c32_0),0);
		}

	if(c1&2){
		gtk_button_set_label(GTK_BUTTON(_n->c32_1),"enabled");
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(_n->c32_1),1);
		}
	else{
		gtk_button_set_label(GTK_BUTTON(_n->c32_1),"disabled");
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(_n->c32_1),0);
		}
		
	if(c1&4){
		gtk_button_set_label(GTK_BUTTON(_n->c32_2),"positive");
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(_n->c32_2),1);
		}
	else{
		gtk_button_set_label(GTK_BUTTON(_n->c32_2),"negative");
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(_n->c32_2),0);
		}
	
	if(c1&8){
		gtk_button_set_label(GTK_BUTTON(_n->c32_3),"enabled");
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(_n->c32_3),1);
		}
	else{
		gtk_button_set_label(GTK_BUTTON(_n->c32_3),"disabled");
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(_n->c32_3),0);
		}
	
	if(c1&16){
		gtk_button_set_label(GTK_BUTTON(_n->c32_4),"enabled");
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(_n->c32_4),1);
		}
	else{
		gtk_button_set_label(GTK_BUTTON(_n->c32_4),"disabled");
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(_n->c32_4),0);
		}
		
	if(c1&32){
		gtk_button_set_label(GTK_BUTTON(_n->c32_5),"disabled");
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(_n->c32_5),1);
		}
	else{
		gtk_button_set_label(GTK_BUTTON(_n->c32_5),"enabled");
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(_n->c32_5),0);
		}		
		
	if(c1&64){
		gtk_button_set_label(GTK_BUTTON(_n->c32_6),"disabled");
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(_n->c32_6),1);
		}
	else{
		gtk_button_set_label(GTK_BUTTON(_n->c32_6),"enabled");
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(_n->c32_6),0);
		}				
	if(c1&128){
		gtk_button_set_label(GTK_BUTTON(_n->c32_7),"clk256B");
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(_n->c32_7),1);
		}
	else{
		gtk_button_set_label(GTK_BUTTON(_n->c32_7),"clk256A");
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(_n->c32_7),0);
		}				

	if(c2&1){
		gtk_button_set_label(GTK_BUTTON(_n->c33_0),"1");
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(_n->c33_0),1);
		}
	else{
		gtk_button_set_label(GTK_BUTTON(_n->c33_0),"0");
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(_n->c33_0),0);
		}

	
	if(c2&2){
		gtk_button_set_label(GTK_BUTTON(_n->c33_1),"1");
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(_n->c33_1),1);
		}
	else{
		gtk_button_set_label(GTK_BUTTON(_n->c33_1),"0");
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(_n->c33_1),0);
		}
	
	if(c2&4){
		gtk_button_set_label(GTK_BUTTON(_n->c33_2),"positive");
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(_n->c33_2),1);
		}
	else{
		gtk_button_set_label(GTK_BUTTON(_n->c33_2),"negative");
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(_n->c33_2),0);
		}
		
	if(c2&8){
		gtk_button_set_label(GTK_BUTTON(_n->c33_3),"clk256A");
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(_n->c33_3),1);
		}
	else{
		gtk_button_set_label(GTK_BUTTON(_n->c33_3),"clk256B");
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(_n->c33_3),0);
		}		
}

static void _nxywidget_config_buttons_clicked( GtkWidget *widget, gpointer   data ){
	int c1,c2;
	NxyWidget *_n = data;
	int c=0;
	int b;
	
	if(_n->_r==NULL)return;
	
	c1=0;
	c2=0;
	//c2 = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(_n->config[1]));
	//c2 = c2&3;
	
	c1 = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(_n->c32_0))<<0;
	c1 += gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(_n->c32_1))<<1;
	c1 += gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(_n->c32_2))<<2;
	c1 += gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(_n->c32_3))<<3;
	c1 += gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(_n->c32_4))<<4;
	c1 += gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(_n->c32_5))<<5;
	c1 += gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(_n->c32_6))<<6;
	c1 += gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(_n->c32_7))<<7;
	
	c2 = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(_n->c33_0))<<0;
	c2 += gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(_n->c33_1))<<1;
	c2 += gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(_n->c33_2))<<2;
	c2 += gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(_n->c33_3))<<3;
	
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(_n->config[0]),c1);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(_n->config[1]),c2);
	
	}

static void _nxywidget_mask_button_clicked( GtkWidget *widget, gpointer data ){
	NxyWidget *_n = data;
	const char *str;
	char *buf;
	int c=-1;
	int num;
	int bit;
	int val;
	
	if(_n->_r==NULL)return;
	
	str = gtk_entry_get_text(GTK_ENTRY(_n->channel_input));
	if(strlen(str)<1)return;
	
	buf = strtok(str,",");
	while(buf!=NULL){
		if(strlen(buf)<1)break;
		c = atoi(buf);			
		if(c>127 || c<0){break;}
		if(c==0){
			if(strlen(buf)>1 || (isdigit(buf[0])==0))break;
			}
		num = (c/8);
		bit = 1<<(c%8);
		val = _n->_r[_n->index].mask[num];
		_n->_r[_n->index].mask[num] = val | bit;
		buf = strtok(NULL,",");
	}
	

	//check for wildcard
	if(strlen(str)==1 && str[0]=='*'){
		for(c=0;c<16;c++){
			_n->_r[_n->index].mask[c]=0xff;
			}
		}
	
	gtk_entry_set_text(GTK_ENTRY(_n->channel_input),"");
	_nxywidget_update(_n);
	}
	
static void _nxywidget_unmask_button_clicked( GtkWidget *widget, gpointer data ){
	NxyWidget *_n = data;
	const char *str;
	char *buf;
	int c=-1;
	int num;
	int bit;
	int val;
	
	if(_n->_r==NULL)return;
	
	str = gtk_entry_get_text(GTK_ENTRY(_n->channel_input));
	if(strlen(str)<1)return;
		
	buf = strtok(str,",");
	while(buf!=NULL){
		if(strlen(buf)<1)break;
		c = atoi(buf);
		if(c>127 || c<0){break;}
		if(c==0){
			if(strlen(buf)>1 || (isdigit(buf[0])==0))break;
			}
		num = (c/8);
		bit = ~(1<<(c%8));
		val = _n->_r[_n->index].mask[num];
		_n->_r[_n->index].mask[num] = val & bit;
		buf = strtok(NULL,",");
		}
	//check for wildcard
	if(strlen(str)==1 && str[0]=='*'){
		for(c=0;c<16;c++){
			_n->_r[_n->index].mask[c]=0;
			}
		}

	gtk_entry_set_text(GTK_ENTRY(_n->channel_input),"");
	_nxywidget_update(_n);
	}	
 
 
 static void _nxywidget_maskreg_button_clicked( GtkWidget *widget, gpointer data ){
	NxyWidget *_n = data;
	const char *str;
	int c=-1;
	int i;
	 
	 
	if(_n->_r==NULL)return;
	str = gtk_entry_get_text(GTK_ENTRY(_n->channel_input));
	if(strlen(str)<1)return;
	
	c = strtoul(str,NULL,0);//atoi(str);
	if(c>255 || c<0)return;
	for(i=0;i<16;i++){
		_n->_r[_n->index].mask[i]=c;
		}
		
	gtk_entry_set_text(GTK_ENTRY(_n->channel_input),"");
	_nxywidget_update(_n);
	
	}                    
	 
	 
	 
static void _nxywidget_th_channel_changed(GtkWidget *widget, gpointer data)
{
	NxyWidget *_n = data;
	int c;
	int v;
	
	if(_n->_r==NULL)return;
	
	c = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(_n->th_channel));
	v = _n->_r->thr[c];
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(_n->th_value),v);
	
	}
	
static void _nxywidget_th_value_changed(GtkWidget *widget, gpointer data)
{
	NxyWidget *_n = data;
	int c;
	int v;
	
	if(_n->_r==NULL)return;
	
	c = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(_n->th_channel));
	v = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(_n->th_value));
	_n->_r->thr[c] = v;
	
	}	

static void _nxywidget_th_value_changed_all(GtkWidget *widget, gpointer data)
{
	NxyWidget *_n = data;
	int c;
	int v;

	if(_n->_r==NULL)return;
	v = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(_n->th_value));
	
	for(c=0;c<128;c++){
		_n->_r->thr[c] = v;
	}
	
	}
