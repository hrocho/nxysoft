/**
 * @file defs.h
 * A.Prochazka
 * last modification 16.10.2014
 * Global variables for nxyter GTK GUI
 */ 

#ifndef DEFS_H
#define DEFS_H

#define DEFAULT_SOCKNAME "x86l-12"
#define SHORTCUTS 


/** global registers structures **/
struct globalreg greg, greg0;
/** nxyter registers structures pointers**/
struct nxyreg *reg=NULL,*reg0=NULL;

/** string to store the name of opened file **/
GString *global_filename; 

/** the main window **/
GtkWidget *global_window;

/** label with SFP configuration **/
GtkWidget *label_sfpconfig; 

/** SFP selector widget **/
GtkWidget *sfp_widget; 
/** EXP selector widget **/
GtkWidget *exp_widget;  
/** NXY selector widget **/
GtkWidget *nxysel_widget; 
/** NXY id # widget **/
GtkWidget *nxycounter_widget;

/** Pre trigger widget **/
GtkWidget *pre_trg_widget;
/** Post trigger widget **/
GtkWidget *post_trg_widget;
/** Test pulse delay widget **/
GtkWidget *test_pulse_delay_widget;
/** Test Trigger delay widget **/
GtkWidget *test_trg_delay_widget;
/** Exploder working mode selector**/
GtkWidget *exploder_mode_widget;

/** Main NxyWidget  **/
GtkWidget *nxyreg_widget;
/** PC name where daemon whould be running **/
GtkWidget *socketname_widget;


#ifdef SHORTCUTS
GtkWidget *sc_input;
GtkWidget *sc_vth;
#endif

#endif
