/**
 * @file
 * A.Prochazka
 * 13.7.2013
 * Helping function for nxyter GTK GUI 
 */ 
#ifndef UTILS_H
#define UTILS_H

#include <gtk/gtk.h>
#include "nxyreg.h"
#include "nxywidget.h"
#include <glib.h>
#include "defs.h"

/**
 * This functions set the label items to show number of nxyter per SFP according to actual globalreg structure
 */ 
void set_sfpconfig_label();

/**
 *This function save the global register setting from widgets to globalreg structure 
 */
void get_global_widgets(struct globalreg *gr);

/**
 *This function set the global register widget to the values supplied by globalreg structure 
 */
void set_global_widgets(struct globalreg *gr);


/**
 * This function is called when general update of all GUI widgets need to be updated
 * Parameters are not used, the function returns nothing
 */ 
void update_general( GtkWidget *widget, gpointer data)
{
    int i;
    set_sfpconfig_label(label_sfpconfig);   	
    
    gtk_spin_button_set_range(GTK_SPIN_BUTTON(sfp_widget),0,3);
    gtk_spin_button_set_range(GTK_SPIN_BUTTON(nxycounter_widget),0,greg.nxy_number-1);
	gtk_spin_button_set_range(GTK_SPIN_BUTTON(exp_widget),0,get_explo_number(greg, reg[0].sfp)-1);
	    	
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(sfp_widget),reg[0].sfp);
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(exp_widget),reg[0].exp);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(nxycounter_widget),0);
    
    			
    nxywidget_set(NXY_WIDGET(nxyreg_widget),reg,0);
    set_global_widgets(&greg);
    
}

/**
 * This function is used to change id of the selected nxyter byt changing the widgets
 * used to select SFP,EXP, NXY or changing NXY id number 
 * if data is 0 sfp,exp or nxy widget is assumed was changed and nxy id is updated
 * if data is 1 nxy id number was changed and other widgets are updated accordingly
 * @param data is used as a identification which widget was updated
 * 
 */ 
void change_current_nxy(GtkWidget *widget, gpointer   data )
{
	int _sfp, _exp, _nxy;
	int index;
	int *c = data;
	
	nxywidget_get(NXY_WIDGET(nxyreg_widget),reg);

	if(*c==1){		//changed sfp,exp or nxy widget
		_sfp = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(sfp_widget));
		_exp = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(exp_widget));
		_nxy = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(nxysel_widget));		
		
		gtk_spin_button_set_range(GTK_SPIN_BUTTON(exp_widget),0,get_explo_number(greg,_sfp)-1);
		gtk_spin_button_set_range(GTK_SPIN_BUTTON(nxysel_widget),0,get_explo_number(greg,_sfp));
	
		index = nxy_map(_sfp,_exp,_nxy,greg);
		if(index>-1){	
			nxywidget_set(NXY_WIDGET(nxyreg_widget),reg,index);
			gtk_spin_button_set_value(GTK_SPIN_BUTTON(nxycounter_widget),index);
			}		
		}
		
	if(*c==0){		//changed by nxy counter
		index = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(nxycounter_widget));
		if(nxy_id(index,&_sfp,&_exp,&_nxy, greg)==0)return;
		
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(sfp_widget),_sfp);
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(exp_widget),_exp);
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(nxysel_widget),_nxy);
			
		nxywidget_set(NXY_WIDGET(nxyreg_widget),reg,index);					
		}		
}

/**
 * Helping function to show dialog with some information
 *  @param data is string to be shown
 */ 
static void show_info(GtkWidget *widget, gpointer data){
	GtkWidget *dialog;
	dialog = gtk_message_dialog_new(GTK_WINDOW(global_window),
									GTK_DIALOG_DESTROY_WITH_PARENT,
									GTK_MESSAGE_INFO,
									GTK_BUTTONS_OK,(char *)data);
	gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);
}

/**
 * This function creates open file dialog to open txt file with nxyter config
 * On success it loads nxyter and global setting to the GUI
 */ 
static void open_file(GtkWidget *widget, gpointer window){
	GtkWidget *dialog;
	gint res;
	int i;
	char *name;
	
	dialog = gtk_file_chooser_dialog_new("Open file ...",
										GTK_WINDOW(global_window),
										GTK_FILE_CHOOSER_ACTION_OPEN,
										GTK_STOCK_CANCEL,
										GTK_RESPONSE_CANCEL,
										GTK_STOCK_OPEN,
										GTK_RESPONSE_ACCEPT,NULL);
										
	res = gtk_dialog_run(GTK_DIALOG(dialog));
	if(res == GTK_RESPONSE_ACCEPT){
		name = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
		g_string_assign(global_filename,name);
		
		free(reg);
		memset(&greg,0,sizeof(struct globalreg));		
		reg = load_nxy_file(name,&greg);
		
		reg0 = (struct nxyreg*) malloc(greg.nxy_number*sizeof(struct nxyreg));
		memcpy(&greg0,&greg,sizeof(struct globalreg));
		memcpy(reg0,reg,sizeof(struct nxyreg));
		
		set_global_widgets(&greg);
		
		if(reg == NULL)
		{
			show_info(dialog,"Error while loading the selected file !");
			memset(&greg,0,sizeof(struct globalreg));
			gtk_widget_destroy(dialog);
			return;
			}
		
		free(name);		
		gtk_widget_destroy(dialog);
	}
	else{
		show_info(dialog,"No file loaded !");gtk_widget_destroy(dialog);return;
		}    	
    	
		update_general(NULL,NULL);
}

/**
 * Function used to save actual nxyter and global configs to the file
 */ 
static void save_file(GtkWidget *widget, gpointer window){
	GtkWidget *dialog;
	gint res;
	char *name;
	
	nxywidget_get(NXY_WIDGET(nxyreg_widget),reg);
	get_global_widgets(&greg);
	if((greg.pre_trg_wind+greg.pos_trg_wind)&1){
	    show_info(NULL,"Wrong Pre and Post trigger window setting \n");
		return ;
	}
	
	dialog = gtk_file_chooser_dialog_new("Save file ...",
										GTK_WINDOW(global_window),
										GTK_FILE_CHOOSER_ACTION_SAVE,
										GTK_STOCK_CANCEL,
										GTK_RESPONSE_CANCEL,
										GTK_STOCK_SAVE,
										GTK_RESPONSE_ACCEPT,NULL);
	gtk_file_chooser_set_do_overwrite_confirmation(GTK_FILE_CHOOSER(dialog), 1);										
	res = gtk_dialog_run(GTK_DIALOG(dialog));
	if(res == GTK_RESPONSE_ACCEPT){
		name = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
		g_string_assign(global_filename,name);
		
		
		if(save_nxy_file(name, greg,reg)!=1)
		{
			show_info(dialog,"Error while saving the file !");
			gtk_widget_destroy(dialog);
			return;
			}
		
		free(name);		
	}
	else{
		show_info(dialog,"File not saved !");
		}
    	gtk_widget_destroy(dialog);
}

/**
 * This function connects to the nxyter daemon and retrieve the nxyter 
 * and global configurations 
 */
static void read_function(GtkWidget *widget, gpointer window){
	const char *_name;
	_name = gtk_entry_get_text(GTK_ENTRY(socketname_widget));
	if(connect_daemon(_name)<0){show_info(NULL,"Could not connect to nxyter daemon");return;}
	if(read_daemon(&reg,&greg)==1){
		update_general(NULL,NULL);
		}
    else{
		show_info(NULL,"Failed to read nxyters.");
		}
								
	close_daemon();
	}
	
/**
 * This function connects to the nxyter daemon and update the nxyter and
 * global configuration to nxyters
 * 
 * 
 */ 	
static void write_function(GtkWidget *widget, gpointer window){
	const char *_name;
	
	_name = gtk_entry_get_text(GTK_ENTRY(socketname_widget));
	nxywidget_get(NXY_WIDGET(nxyreg_widget),reg);
	get_global_widgets(&greg);
	
	if((greg.pre_trg_wind+greg.pos_trg_wind)&1){
	    show_info(NULL,"Wrong Pre and Post trigger window setting \n");
	    }
	else{
	    if(connect_daemon(_name)<0){show_info(NULL,"Could not connect to nxyter daemon");return;}
	
    if(update_daemon(reg,greg,255)<1){
        show_info(NULL,"Failed to write to nxyters.");
        }
	close_daemon();	
	}
	}	


/**
 * This is helping function to adjust label widget 
 */ 
void set_sfpconfig_label(){
	GString *txt;
	int i,s;
	
	txt = g_string_sized_new(200);
	
	for(s=0;s<MAX_SFP;s++){
		g_string_append_printf(txt," SFP%d:",s);
		for(i=0;i<MAX_EXP;i++)
		{
			if(greg.sfp_in_use[s][i])
				g_string_append_printf(txt," %d",greg.sfp_in_use[s][i]);
			}
		g_string_append_printf(txt,"\n");
		}
	
    gtk_label_set_text(GTK_LABEL(label_sfpconfig),txt->str);
	g_string_free(txt,1);
}

/**
 * Helping function to set global widget
 */ 
void set_global_widgets(struct globalreg *gr){
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(pre_trg_widget),gr->pre_trg_wind);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(post_trg_widget),gr->pos_trg_wind);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(test_pulse_delay_widget),gr->test_pul_del);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(test_trg_delay_widget),gr->test_trg_del);
	if(gr->mode == 1){
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(exploder_mode_widget),1);
		gtk_button_set_label(GTK_BUTTON(exploder_mode_widget),"test mode");
		}
	else{
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(exploder_mode_widget),0);
		gtk_button_set_label(GTK_BUTTON(exploder_mode_widget),"normal mode");
		}
	
	}

/**
 * Helping function to get global widget
 */ 
void get_global_widgets(struct globalreg *gr){
	gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(pre_trg_widget));
	gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(post_trg_widget));
	gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(test_pulse_delay_widget));
	gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(test_trg_delay_widget));
	gr->pre_trg_wind = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(pre_trg_widget));
	gr->pos_trg_wind = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(post_trg_widget));
	gr->test_pul_del = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(test_pulse_delay_widget));
	gr->test_trg_del = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(test_trg_delay_widget));
	gr->mode = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(exploder_mode_widget));
	}
	
/**
 * Helping function to update global widgets
 */ 	
void change_global_widgets(GtkWidget *widget, gpointer   data){
	get_global_widgets(&greg);
	set_global_widgets(&greg);
	}
	
/**
 * 
 */
void sc_vth_clicked(GtkWidget *widget, gpointer   data){
	int n,i;
	n = greg.nxy_number;
	const char *str;
	int c=-1;
	
	str = gtk_entry_get_text(GTK_ENTRY(sc_input));
	if(strlen(str)<1)return;
	c = strtoul(str,NULL,0);
	if(c>255 || c<0)return;
	
	for(i=0;i<n;i++){
		reg[i].bias[2]=c;
		}
	
	update_general(NULL,NULL);
	}
 
  	
#endif
