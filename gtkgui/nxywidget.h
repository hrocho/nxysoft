/**
 * @file
 * A.Prochazka
 * 13.7.2013
 * Header file for custom GTK widget to show registers of nxyter
 * 
 */ 
#ifndef _NXY_WIDGETS_H_
#define _NXY_WIDGETS_H_

#include <glib.h>
#include <gtk/gtk.h>
#include "nxyreg.h"
G_BEGIN_DECLS

#define NXY_WIDGET_TYPE (nxywidget_get_type())
#define NXY_WIDGET(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj),NXY_WIDGET_TYPE, NxyWidget))
#define NXY_WIDGET_CLASS(obj) (G_TYPE_CHECK_CLASS_CAST ((obj),NXY_WIDGET_TYPE, NxyWidgetClass))
#define IS_NXY_WIDGET(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj),NXY_WIDGET_TYPE))
#define IS_NXY_WIDGET_CLASS(obj) (G_TYPE_CHECK_CLASS_TYPE ((obj),NXY_WIDGET_TYPE))

typedef struct _NxyWidget NxyWidget;
typedef struct _NxyWidgetClass NxyWidgetClass;

/**
 * Structure group widget used to show register values and to store data
 */ 
struct _NxyWidget
{
	GtkVBox container;
	GtkWidget *bias[14]; 
	
	GtkWidget *masked_channels;
	GtkWidget *channel_input;
	GtkWidget *mask_button;
	GtkWidget *unmask_button;
	GtkWidget *maskreg_button;

	GtkWidget *config[2];
	GtkWidget *c32_0;
	GtkWidget *c32_1;
	GtkWidget *c32_2;
	GtkWidget *c32_3;
	GtkWidget *c32_4;
	GtkWidget *c32_5;
	GtkWidget *c32_6;
	GtkWidget *c32_7;
	GtkWidget *c33_0;
	GtkWidget *c33_1;
	GtkWidget *c33_2;
	GtkWidget *c33_3;
	
	GtkWidget *te_del[2];
	GtkWidget *clk_del[3];
	
	GtkWidget *th_channel;
	GtkWidget *th_value;
	GtkWidget *th_te;		
	GtkWidget *th_all_button;
	
	/** tabs - notebooks **/
	GtkWidget *nb;
	
	/** pointer to nxyreg structure from where actual nxyter is shown **/
	struct nxyreg *_r; 
	
	/** index of nxyter which is actually shown **/
	int index; 
	};
	
struct _NxyWidgetClass
{
	GtkVBoxClass parent_class;
	
	void (*nxywidget) (NxyWidget *nxy);
	};	

/**
 * nxywidget_new initializes the widget
 * @return pointer to the new NxyWidget 
 */ 
GtkWidget* nxywidget_new(void);

/**
 * nxywidget_set is used to load nxyreg structure to nxywidget.
 * @param *n Pointer to NxyWidget
 * @param *r is ponter to nxyreg structure to be shown
 * @param index is the number of nxyter to be shown from the structure pointed by r
 */ 
void nxywidget_set(NxyWidget *n, struct nxyreg *r,int index);

/**
 * nxywidget_get is used to save nxyreg structure from nxywidget.
 * @param *n Pointer to NxyWidget
 * @param *r is ponter to nxyreg structure where the structure will be saved
  */ 
int nxywidget_get(NxyWidget *n, struct nxyreg *r);

/**
 * nxywidget_get_index returns the index (number) of the currently shown nxyter
 * @param *n Pointer to NxyWidget
 * @return nxyter number	 
 */ 
int nxywidget_get_index(NxyWidget *n);

GType nxywidget_get_type(void)G_GNUC_CONST;

G_END_DECLS
#endif
