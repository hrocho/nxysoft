/**
 * @file
 * A. Prochazka 
 * 13.7.2013
 * 
 * set of function used for calibration, readout and manipulation of nxyters 
 * from MBS
 */
  
#include "nxyjobs.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

void nxydata_fill(int _sfp,int _exp, int _nxy, int _ch, int _adc);
int nxydata_unpack(long *pdata);
double nxyint_mean(int *baseline);
double nxyhits_mean(struct nxyhits *n, int ch);
double nxyhits_median(struct nxyhits *n, int ch);
double nxyhits_nhitmean(struct nxyhits *n);
int compare(const void *a, const void *b);
int nxyint_maxindex(int *b, int num);
int nxyint_minindex(int *b, int num);
int nxyint_closestindex(int *b, int aim, int num);
double nxyint_rms(int *b, int num);
/////////////////// Nxy data initializtion /////////////////////////////////
int nxydata_init(char *_socket_name){
	#ifdef DEBUG
	printm("nxydata_init: initializing data\n");
	#endif
	_r = NULL;
	
	nxy_socket_name = _socket_name;
	nxydata.nxy_number = _greg0.nxy_number;
	nxydata.phits = (struct nxyhits*)malloc(nxydata.nxy_number*sizeof(struct nxyhits));
	nxydata_results = (struct nxyres1 *)malloc(nxydata.nxy_number*sizeof(struct nxyres1));
	if(nxydata.phits==NULL || nxydata_results==NULL)return 0;

	nxydata_event_counter = 0;
	nxydata_ready = 0;
	nxydata_status = 0;
	nxydata_job_stage = 0;
	memset(&nxyjobs,0,sizeof(struct _nxyjobs));
	return 1;
}

//////////// clean initialized data ///////////////
int nxydata_clean(){
	free(nxydata.phits);
	return 1;
}


///////////// reset nxy data /////////////////
void nxydata_reset(){
	memset(nxydata.phits,0,nxydata.nxy_number*sizeof(struct nxyhits));
	nxydata_event_counter=0;
	nxydata_ready=0;
}



///////////// nxyjob_set //////////////////
int nxyjob_set(){
	nxyjobs_t d;
	int i;

	
	d = nxyjobs.q[0];	
	if(d==0){
		nxydata_job_stage=0;
		return 0;
		}
	#ifdef DEBUG
	printm("\n------------ nxyjob_set: switching to job %d\n",d);
	#endif
	switch(d){

		case NXYJOB_VBIASS_SCAN : {
					nxyjob_function_init = &nxyjob_vbiass_init;
					nxyjob_function_main = &nxyjob_vbiass_main;
					nxyjob_function_done = &nxyjob_vbiass_done;
					nxydata_job_stage=1;	
					break;
					}

		case NXYJOB_VTH_SCAN : {
					nxyjob_function_init = &nxyjob_vth_init;
					nxyjob_function_main = &nxyjob_vth_main;
					nxyjob_function_done = &nxyjob_vth_done; 	
					nxydata_job_stage=1;	
					break;
					}
		case NXYJOB_LOCALTH_SCAN : {
					nxyjob_function_init = &nxyjob_lth_init;
					nxyjob_function_main = &nxyjob_lth_main;
					nxyjob_function_done = &nxyjob_lth_done; 	
					nxydata_job_stage=1;	
					break;
					}
					
		case NXYJOB_BASELINES_READ : {
					nxyjob_function_init = &nxyjob_baselines_init;
					nxyjob_function_main = &nxyjob_baselines_main;
					nxyjob_function_done = &nxyjob_baselines_done; 	
					nxydata_job_stage=1;	
					break;
					}
		default: return 0;	
		}

	for(i=1;i<10;i++){
		if(nxyjobs.q[i]==0){nxyjobs.q[i-1]=0;break;}
		nxyjobs.q[i-1]=nxyjobs.q[i];
	}

	return 1;
}



//////////  nxyjob_process    ////////////
int nxyjob_process(long *_pd){
	if(nxydata_job_stage>0){
		if(nxydata_job_stage == 2 && nxydata_ready==0){
			nxydata_unpack(_pd);
		}
		else{ //action after data were collected 
			if(nxydata_job_stage == 1){
				nxydata_status = nxyjob_function_init();
				if(nxydata_status==1){
					nxydata_job_stage=2;
					nxydata_status=0;
					nxydata_reset();						
					return 1;
					}
				}

			if(nxydata_job_stage == 2 ){
				nxydata_status = nxyjob_function_main();
				if(nxydata_status){
					nxydata_job_stage = 3;	
					nxydata_status=0;
					}
				nxydata_reset();				
				}
			
			if(nxydata_job_stage == 3){
				nxydata_status = nxyjob_function_done();
				if(nxydata_status == 1){
					nxydata_status=0;
					nxyjob_set();
					}
				}
			}
		}
		
	return 1;
}


////////// nxyjob_add //////////////
int nxyjob_add(nxyjobs_t j){
	int i;
	
	for(i=0;i<10;i++){
		if(nxyjobs.q[i]==0)break;
		}
	if(i>=10)return 0;
	nxyjobs.q[i]=j;
	if(i==0 && nxydata_job_stage==0)nxyjob_set();
	return 1;
}

//////////////////////// Vbias scan functions ///////////////////////////
int nxyjob_vbiass_init(){
	int i;
	
	#ifdef DEBUG
	printm("VBiaS scan initializing \n");
	#endif
	nxydata_job_stage=1;
	nxyregcpy(&_r,&_r0,&_greg,&_greg0);
	if(_r==NULL){printf("not allocated\n");exit(1);}
	for(i=0;i<_greg0.nxy_number;i++)	
	{
		_r[i].bias[5]=1;					
		_r[i].config[0]=8;
	}
	_greg.mode = 1;
	connect_daemon(nxy_socket_name);
	i = update_daemon(_r,_greg,NXY_CONFIG|NXY_BIAS|NXY_RECEIVER);	
	close_daemon();	
	return i;
} 

int nxyjob_vbiass_main(){
	int i,j;
	int c;

	c=_r[0].bias[5];

	#ifdef DEBUG
	printm("VBiaS scan: scanning %d value.\n",c);
	#endif

	for(i=0;i<_greg0.nxy_number;i++)	
	{
		for(j=0;j<128;j++){
			nxydata_results[i].data[c][j]=(int)nxyhits_median(&nxydata.phits[i],j);
			}
		nxydata_results[i].mean[c] = nxyint_mean(nxydata_results[i].data[c]); 
		_r[i].bias[5]++;					
	}
	
	if(c==255){
		return 1;
	}
	connect_daemon(nxy_socket_name);
	i = update_daemon(_r,_greg,NXY_BIAS);

	if(i!=1){
		for(i=0;i<_greg0.nxy_number;i++)	
			_r[i].bias[5]--;					
	}	
	close_daemon();	

	return 0;
}

int nxyjob_vbiass_done(){
	int i,j,k,c=0;
	char buf[20];
	struct nxyres1 *p = nxydata_results;

	#ifdef DEBUG
	printm("VBiaS scan finished.\n");
	#endif
	
	for(i=0;i<_greg0.nxy_number;i++){
		int dif = 0;
		int pol = 0;
		FILE *fw,*fw2;
	
		if((_r0[i].config[1]&4)==4)pol=1; //check polarity setting
		#ifdef DEBUG
		if(pol==1)printm("configuring for positive polarity\n");
		else printm("configuring for negative polarity\n");
		#endif
		/// scan through values and check where its start to increase
		for(j=254;j>0;j--){
			if(pol==1)k = 255-j;
			else k=j;
			if(p[i].mean[k]!=0 && p[i].mean[k+1]!=0){
				dif = p[i].mean[k]-p[i].mean[k+1];
				if(pol==1)dif=-dif; //to have the same difference sign as negative polarity
				}
			else dif=0;
			if(dif<-5){
				j--;
				if(pol==1)k = 255-j;
				else k=j;
				dif = p[i].mean[k]-p[i].mean[k+1];
				if(pol==1)dif=-dif; 
				if(dif<-5){c=k;break;} ///2nd difference bigger than 5, lets take the value
				}
		}
		#ifdef DEBUG
		printm("VbiasS: nxy%d -> value = %d\n",i,c);
		#endif
		_r0[i].bias[5]=c;		
		sprintf(buf,"dvbias_%d.txt",i);
		fw = fopen(buf,"w");
		sprintf(buf,"dvbias_%d_full.txt",i);
		fw2 = fopen(buf,"w");
				
		for(j=0;j<256;j++){
			if(p[i].mean[j]){
				for(k=0;k<128;k++)
					fprintf(fw2,"%d %d %d %d\n",i,k,j,p[i].data[j][k]);
				fprintf(fw,"%d %d %d\n",i, j,p[i].mean[j]);
				}
			}
			
		fclose(fw);				
		fclose(fw2);
		
	}//nxy loop
	
	connect_daemon(nxy_socket_name);
	i = update_daemon(_r0,_greg0,NXY_CONFIG|NXY_BIAS|NXY_RECEIVER);
	close_daemon();
	
	return i;
}
//////////////////////////// Vth scan functions ////////////////////////////////////
int nxyjob_vth_init(){
	int i;
	
	#ifdef DEBUG
	printm("Vth scan initializing \n");
	#endif
	nxydata_job_stage=1;
	nxyregcpy(&_r,&_r0,&_greg,&_greg0);
	if(_r==NULL){printf("not allocated\n");exit(1);}
	for(i=0;i<_greg0.nxy_number;i++)	
	{
		_r[i].bias[2]=1;					
		_r[i].config[0]=0;
	}
	
	_greg.mode = 0;
	connect_daemon(nxy_socket_name);
	i = update_daemon(_r,_greg,NXY_CONFIG|NXY_BIAS|NXY_RECEIVER);	
	close_daemon();	
	

	return i;
} 



int nxyjob_vth_main(){
	int i;
	int j;
	int c;

	c=_r[0].bias[2];

	#ifdef DEBUG
	printm("Vth scan: scanning %d value.\n",c);
	#endif

	for(i=0;i<_greg0.nxy_number;i++)	
	{
		for(j=0;j<128;j++){
			nxydata_results[i].data[c][j]=nxydata.phits[i].num[j];
		}
	
		nxydata_results[i].mean[c]=nxyhits_nhitmean(&nxydata.phits[i]);
		_r[i].bias[2]++;					
	}

	if(c==255){
		return 1;
	}
	connect_daemon(nxy_socket_name);
	i = update_daemon(_r,_greg,NXY_BIAS);


	if(i!=1){
		for(i=0;i<_greg0.nxy_number;i++)	
			_r[i].bias[2]--;					
	}	
	close_daemon();	

	return 0;
}

int nxyjob_vth_done(){
	int i,j,k;
	char buf[20];

	#ifdef DEBUG
	printm("VTh scan finished.\n");
	#endif
	
	for(i=0;i<_greg0.nxy_number;i++){
		FILE *fw;
		sprintf(buf,"dvth_%d.txt",i);
		fw = fopen(buf,"w");

		for(j=1;j<255;j++){
			for(k=0;k<128;k++){
				fprintf(fw,"%d %d %d %d\n",i, j,k,nxydata_results[i].data[j][k]);
				}
			if(j>7 && nxydata_results[i].mean[j]==0){
				break;
				}
			}
			_r0[i].bias[2]=j;
		
		#ifdef DEBUG
		printm("Vth: nxy%d -> value = %d\n",i,j);
		#endif
		fclose(fw);				

	}//nxy loop
	
	connect_daemon(nxy_socket_name);
	i = update_daemon(_r0,_greg0,NXY_CONFIG|NXY_BIAS|NXY_RECEIVER);
	close_daemon();
	
	return i;
}


//////////////////////////// local thresholds ////////////////////////////////////
int nxyjob_lth_init(){
	int i,j;
	
	#ifdef DEBUG
	printm("Localth scan initializing \n");
	#endif
	nxydata_job_stage=1;
	nxydata_status=0;
	nxyregcpy(&_r,&_r0,&_greg,&_greg0);
	if(_r==NULL){printf("not allocated\n");exit(1);}
	for(i=0;i<_greg0.nxy_number;i++)	
	{
		_r[i].bias[2]=30;					
		_r[i].config[0]=0;
		for (j = 0; j < 128; j++)
		{
			_r[i].thr[j]=21;
		}
		
	}
	
	_greg.mode = 0;
	connect_daemon(nxy_socket_name);
	i = update_daemon(_r,_greg,NXY_CONFIG|NXY_BIAS|NXY_RECEIVER|NXY_TH);	
	close_daemon();	

	memset(nxydata_results,0,nxydata.nxy_number*sizeof(struct nxyres1));

	return i;
} 



int nxyjob_lth_main(){
	int i;
	int j;
	int c;
	int c2;
	int sum;
	int *s;
	s = &nxydata_results[0].mean[255];

	#ifdef DEBUG
	printm("local th begining, status = %d \n",*s);
	#endif

	sum=1;
	c2=_r[0].thr[0];
	c=_r[0].bias[2];
	for(i=0;i<_greg0.nxy_number;i++)	
	{
		for(j=0;j<128;j++){
			nxydata_results[i].data[c2][j]=nxydata.phits[i].num[j];
		}
		nxydata_results[i].mean[c]=nxyhits_nhitmean(&nxydata.phits[i]);
		printm("results: %i -> %d \n",i,nxydata_results[i].mean[c]);
	}

	if(c2==31)return 1;

	if(*s==0){
		if(c<5){
			*s=1;			
			for(i=0;i<_greg0.nxy_number;i++){
				_r[i].bias[2]=nxyint_maxindex(nxydata_results[i].mean,30)+1;
				printm("%i -> selected %d \n",i,_r[i].bias[2]);
				for(j=0;j<128;j++){
					_r[i].thr[j]=1;
					}
				}
		}
		else{
			for(i=0;i<_greg0.nxy_number;i++)	
				_r[i].bias[2]--;	
				printm("vth changed to %d\n",c);
			}
		connect_daemon(nxy_socket_name);
		i = update_daemon(_r,_greg,NXY_BIAS|NXY_TH);
		if(i!=1){
			for(i=0;i<_greg0.nxy_number;i++)	
				_r[i].bias[2]++;					
		}	
		close_daemon();

		return 0;		
	}
	
	if(*s==1){
		#ifdef DEBUG
		printm("local th : changing to %d value \n",c+1);
		#endif
		for(i=0;i<_greg0.nxy_number;i++)
			for(j=0;j<128;j++){
				_r[i].thr[j]++;
				}
			
	connect_daemon(nxy_socket_name);
	i = update_daemon(_r,_greg,NXY_BIAS|NXY_TH);

	if(i!=1){
		for(i=0;i<_greg0.nxy_number;i++)	
			_r[i].bias[2]++;					
	}	
	close_daemon();	
	}
		
	return 0;
}

int nxyjob_lth_done(){
	int i,j,k,c;
	char buf[20];
	int d[32];
	double dd;

	#ifdef DEBUG
	printm("Local Th scan finished.\n");
	#endif
	
	for(i=0;i<_greg0.nxy_number;i++){
		FILE *fw,*ftemp;
		double min=999999999;
		int _index;
		sprintf(buf,"dlocalth_%d.txt",i);
		fw = fopen(buf,"w");
		sprintf(buf,"log_%d.txt",i);
		ftemp = fopen(buf,"w");

		c = _r[i].bias[2];

		for(k=0;k<128;k++){
			for(j=1;j<32;j++){
				fprintf(fw,"%d %d %d %d\n",i, j,k,nxydata_results[i].data[j][k]);	
			}
		}

		for(j=1;j<32;j++){
			dd = nxyint_rms(nxydata_results[i].data[j],128);
			if(dd<min && dd > 0){min=dd;_index=j;}
			printm("rms %d %lf\n",j,dd);
			}
		dd = nxyint_mean(nxydata_results[i].data[_index]);
		printm("aimed mean = %lf\n",dd);
		fprintf(ftemp,"aimed mean = %lf\n",dd);
		for(k=0;k<128;k++){
			fprintf(ftemp,"------ channel %d -------\n",k);
			min=999999999;
			for(j=1;j<32;j++){
				fprintf(ftemp,"reg  = %d, n = %d\n",j,nxydata_results[i].data[j][k]);
				if(abs(nxydata_results[i].data[j][k]-dd)<min && nxydata_results[i].data[j][k]>0){
					min = abs(nxydata_results[i].data[j][k]-dd);
					c = j;
					fprintf(ftemp, "*** value closer to aim found, dist = %lf",min);
					}
				}
			_r0[i].thr[k]=c;
			printm("%d %d -> %d = %lf - %d\n",i,k,c,dd,nxydata_results[i].data[c][k]);
			fprintf(ftemp,"*** value taken = %d\n",c);
			}
			
		#ifdef DEBUG
		printm("Vth: nxy%d -> min rms value = %d\n",i,_index);
		#endif
		fclose(fw);			
		fclose(ftemp);
		

	}//nxy loop	

	connect_daemon(nxy_socket_name);
	i = update_daemon(_r0,_greg0,NXY_CONFIG|NXY_BIAS|NXY_RECEIVER|NXY_TH);
	close_daemon();

	
	return i;
}


///////////////// Baseline reader jobs ////////////////////////////////////////////
int nxyjob_baselines_init(){
	int i;
	
	#ifdef DEBUG
	printm("Baseline read initializing \n");
	#endif
	nxydata_job_stage=1;

	connect_daemon(nxy_socket_name);
	read_daemon(&_r0,&_greg0);
	close_daemon();
	nxyregcpy(&_r,&_r0,&_greg,&_greg0);
	for(i=0;i<_greg0.nxy_number;i++)	
	{
		_r[i].config[0]=8;
	}

	_greg.mode = 1;

	connect_daemon(nxy_socket_name);
	i = update_daemon(_r,_greg,NXY_RECEIVER|NXY_CONFIG);	
	close_daemon();	
	return i;
} 

int nxyjob_baselines_main(){
	int i,j;

	#ifdef DEBUG
	printm("Baseline read\n");
	#endif

	for(i=0;i<_greg0.nxy_number;i++)	
	{
		for(j=0;j<128;j++){
			nxydata_results[i].data[0][j]=(int)nxyhits_median(&nxydata.phits[i],j);
			}
	}

	return 1;
}

int nxyjob_baselines_done(){
	int i,j;
	FILE *fw;
	
	fw = fopen("baselines.txt","w");
	for(i=0;i<_greg0.nxy_number;i++){
		for(j=0;j<128;j++){
			fprintf(fw,"%d %d %d %d %d\n",_r0[i].sfp, _r0[i].exp, _r0[i].nxy,j,nxydata_results[i].data[0][j]);
		}
	}

	

	connect_daemon(nxy_socket_name);
	i = update_daemon(_r0,_greg0,NXY_CONFIG | NXY_RECEIVER);			
	close_daemon();	
	fclose(fw);
	return i;
}
/////////////////////////////////////////


void nxydata_fill(int _sfp,int _exp, int _nxy, int _ch, int _adc){
	int index;
	if(_ch<0 || _ch>128)return;
	if(_sfp<0 || _sfp>MAX_SFP)return;
	if(_exp<0 || _exp>MAX_EXP)return;
	if(_nxy<0 || _nxy>2)return;
	if(_adc<1 || _adc>4095)return;
	
	index = nxy_map(_sfp,_exp,_nxy,_greg0);
	if(nxydata.phits[index].num[_ch]<MAXSTAT)
	nxydata.phits[index].hits[_ch][nxydata.phits[index].num[_ch]] = _adc;
	nxydata.phits[index].num[_ch]++;
}

//////////////// nxydata_unpack ///////////////////////
int nxydata_unpack(long *pdata){
	unsigned int first,second;
	int nxy_n_cha;
	int i,j,k,l;
	int ch, adc;

	if ( (*pdata)==0xbad00bad )
  	{
    		printm ("found bad event \n");
	    	return 0;
  	}

	for(i=0;i<MAX_SFP;i++){
		if(_greg0.sfp_in_use[i][0]<1)continue;
		
		//skip padding
		for(j=0;j<100;j++){
			first = *pdata++;
			if( (first&0xfff00000) != 0xadd00000){
				pdata--;
				break;
				}
			}
		for(j=0;j<MAX_EXP;j++){
			if(_greg0.sfp_in_use[i][j]==0)break;
		
			for(k=0;k<_greg0.sfp_in_use[i][j];k++){
				///exploder header
				first = *pdata++;
				if( ((first & 0xff) >> 0) != 0x34 ){
            				printm ("ERROR>> channel header type is not 0x34 \n");
		            		return 0;
          			}
				if( ((first & 0xff0000) >> 16) != j ){
            				printm ("ERROR>> exploder id is wrong \n");
		            		return 0;
	          		}
		    		if( ((first & 0xff000000) >> 24) != k )
				{
            				printm ("ERROR>> Nxyter id is wrong \n");
		            		return 0;
	          		}

				///data size
				first = *pdata++;
				nxy_n_cha = (first - 20) >> 3;
				
				///nxy header
				first = *pdata++;
				if( ((first & 0xff000000) >> 24) != 0xaa)
				{
		            		printm ("ERROR>> Nxyter header id is not 0xaa \n");
		         		return 0;
          			}

				////////////////////////////////////////////////////////
				/// trigger counter from nxy header can be read here ///
				////////////////////////////////////////////////////////
				
				first = *pdata++;
				second = *pdata++;

				////////////////////////////////////////////////////////
				/// epoch time should be read here ///
				////////////////////////////////////////////////////////
				
				for(l=0;l<nxy_n_cha;l++){
					first = *pdata++;
					second = *pdata++;

					ch = (second & 0x7f000000) >> 24;
					adc = (first & 0xfff0000) >> 16;
					nxydata_fill(i,j,k,ch,adc);
				}///end of channels loop

				///error pattern
				first = *pdata++;
				if( ((first & 0xff000000) >> 24) != 0xee)
				{
					printm ("ERROR>> Nxyter error pattern is not 0xee \n");
					return -1;
          			}
          		
	          		///nxy trailer
	          		first = *pdata++;
	          		if( ((first & 0xff000000) >> 24) != 0xbb)
				{
		            		printm ("ERROR>> Nxyter trailer id is not 0xbb \n");
		            		return -1;
	          		}
				
			} ///loop over NXY
		}/// loop over EXP

	} //end SFP loop

	nxydata_event_counter++;
	if(nxydata_event_counter>=MAXSTAT)nxydata_ready = 1;
	
	return nxydata_event_counter;
}

int compare(const void *a, const void *b)
{
	return (*(int*)a -*(int*)b);
	}

double nxyhits_mean(struct nxyhits *n, int ch){
    int sum=0;
    int i;

    if(n->num[ch]==0)return -1;
    if(ch>128 || ch<0)return -1;
    for(i=0;i<n->num[ch];i++)
    {
        sum+=n->hits[ch][i];
    }

    return (double)sum/n->num[ch];
}

double nxyhits_median(struct nxyhits *n, int ch){

    if(n->num[ch]==0)return 0;
    if(ch>128 || ch<0)return -1;
    qsort(n->hits[ch],n->num[ch],sizeof(int),compare);

    return (double)0.5*(n->hits[ch][(n->num[ch])/2] + n->hits[ch][1+(n->num[ch])/2]);
}


double nxyhits_nhitmean(struct nxyhits *n){
    int sum=0;
    int i;

    for(i=0;i<128;i++)
    {
        sum+=n->num[i];
    }

    return (double)sum/128.0;
}

double nxyint_mean(int *b){
    int sum = 0;
    int i=0,n=0;

    for(i=0;i<128;i++){
        sum+=b[i];
        if(b[i]>0)n++;
    }
    if(n==0)return 0;
    return (double)sum/128.0;
}

double nxyint_rms(int *b, int num){
	int i;
	double sum = 0;
	int mean = 0;
	int count=0;

	for(i=0;i<num;i++){
	        if(b[i]>0){
			sum+=b[i];
			count++;
			}
	    }
	if(count==0)return -1;
	if(count<70)return -1;
	mean = sum/count;

	sum=0;count=0;
	for(i=0;i<num;i++){
		if(b[i]>0){
		sum+=(b[i]-mean)*(b[i]-mean);
		count++;
		}
	}
	sum = sum/count;
	sum = sqrt(sum);
	return sum;
}

int nxyint_maxindex(int *b, int num){
	int i,index;
	int max;

	max = b[0];
	index = 0;
	for(i=1;i<num;i++){
		if(b[i]>max)
			{
			max = b[i];
			index = i;
			}
		}
	return index;
}

int nxyint_minindex(int *b, int num){
	int i,index;
	int min;

	min = b[0];
	index = 0;
	for(i=1;i<num;i++){
		if(b[i]<min)
			{
			min = b[i];
			index = i;
			}
		}
	return index;
}


int nxyint_closestindex(int *b, int aim, int num){
	int index;
	int dif;
	int i;

	index = 0;
	dif = abs(b[0]-aim);
	for(i=1;i<num;i++){
		if(abs(b[i]-aim)<dif){
			dif = abs(b[i]-aim);
			index = i;
		}
	}
	
	return index;
}

