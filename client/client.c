/// A. Prochazka
/// basic client for communicating with nxy daemon
/// last update 5.6.2013
/// last added - saving file after reading
/// need nxyreg

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h> //posix api
#include <errno.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <netdb.h>
#include <arpa/inet.h>

#include "nxyreg.h"


int main(int argc, char *argv[])
{
	const char* const socket_name = argv[1];
	char* filename = "nxyconfig.txt";
	int socket_fd;
	struct addrinfo hints, *servinfo, *p;
	int status;
	char message[3];
	int length=0;
    char *buf=NULL,*save=NULL;
    int dummy;
    int com=0,i=0;
    int data_send=0;
    int data_receive=0;
    int data_reset = 0;
    int data_save = 0;
    char *socket_port="3490";

    struct nxyreg *r=NULL;
    struct globalreg greg;
	printf("sizes = %d %d\n",sizeof(struct globalreg),sizeof(struct nxyreg));


	memset(&hints,0,sizeof(hints));
    memset(&greg,0,sizeof(greg));

    /// arguments handling
    com = 255;
    if(argc<3){printf("Wrong arguments!\n");exit(1);}
    for(i=2;i<argc;i++){
        if(strcmp(argv[i],"W")==0){
            com = com&0xffffff;
            com+=(int)'W'<<24;
            data_send = 1;
            data_receive = 0;
        }
        
        if(strcmp(argv[i],"w")==0){
            com = com&0xffffff;
            com+=(int)'w'<<24;
            data_send = 1;
            data_receive = 0;
        }

        if(strcmp(argv[i],"R")==0){
            com = com&0xffffff;
            com+=(int)'R'<<24;
            data_receive = 1;
            data_send = 0;
        }
        
        if(strcmp(argv[i],"r")==0){
            com = com&0xffffff;
            com+=(int)'r'<<24;
            data_receive = 1;
            data_send = 0;
        }
	
        if(strcmp(argv[i],"F")==0){
            com = com&0xffffff;
            com+=(int)'F'<<24;
            data_receive = 1;
            data_send = 0;
        }


        if(strcmp(argv[i],"T")==0){
            com = com&0xffffff;
            com+=(int)'T'<<24;
            data_send = 1;
            data_receive = 0;
        }
        
        if(strcmp(argv[i],"Q")==0){
            com = com&0xffffff;
            com+=(int)'Q'<<24;
            data_send = 0;
            data_receive = 0;
        }

        if(strcmp(argv[i],"B")==0){
            com = com&0xffffff;
            com+=(int)'B'<<24;
            data_send = 0;
            data_receive = 0;
        }
/*
        if(strcmp(argv[i],"L")==0){
            com = com&0xffffff;
            com+=(int)'L'<<24;
            data_send = 0;
            data_receive = 0;
        }

        if(strcmp(argv[i],"U")==0){
            com = com&0xffffff;
            com+=(int)'U'<<24;
            data_send = 0;
            data_receive = 0;
        }
*/



        if((strcmp(argv[i],"--reset")==0) || (strcmp(argv[i],"-r")==0)){
            com+=(255<<16);
            data_reset = 1;
        }

                
        if((strcmp(argv[i],"--config")==0) || (strcmp(argv[i],"-c")==0)){
            if(i+1>=argc){printf("wrong arguments \n");exit(1);}
            filename = argv[i+1];
            i++;
        }
        
        if((strcmp(argv[i],"--save")==0) || (strcmp(argv[i],"-s")==0)){
            if(i+1>=argc){printf("wrong arguments \n");exit(1);}
            filename = argv[i+1];
            i++;
            data_save = 1;
        }
        
        if((strcmp(argv[i],"--port")==0) || (strcmp(argv[i],"-p")==0)){
            if(i+1>=argc){printf("wrong arguments \n");exit(1);}
            socket_port = argv[i+1];
            i++;
            
        }

    }

    switch((com&0xff000000)>>24)
    {
        case 'R': printf("reading operation \n");break;
        case 'r': printf("reading operation \n");break;
		case 'F': printf("forced reading operation \n");break;
        case 'W': printf("writing operation \n");break;
        case 'w': printf("writing operation \n");break;
        case 'T': printf("test mode operation\n");break;
        case 'Q': printf("killing daemon\n");break;
		case 'B': printf("ending session\n");break;
		case 'L': printf("locking nxyters\n");break;
		case 'U': printf("unlocking nxyters\n");break;
        default:  printf("invalid command \n");exit(1);
    }

    switch((com&0xff0000)>>16)
    {
        case 255: printf("nxy reset selected\n");break;
        case 0: printf("without nxy reset\n");break;
        default:  printf("invalid command \n");exit(1);
    }

    if((com&0xff)==255)printf("all register will updated\n");
    if(data_save == 1 && data_receive!=1){
    	data_save = 0;
    	printf("error: saving file is supported only for reading command \n");
    	}


    /// connecting to network socket
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;

	status = getaddrinfo(socket_name,socket_port,&hints, &servinfo);
	if(status!=0){printf("getaddrinfo error: %s\n",gai_strerror(status));}

	for(p = servinfo;p!=NULL;p=p->ai_next){
		socket_fd = socket(p->ai_family,p->ai_socktype,p->ai_protocol);
		if(socket_fd<0){printf("socket error");continue;}

		status = connect(socket_fd,p->ai_addr,p->ai_addrlen);
		if(status<0){
			close(socket_fd);printf("connect error\n");continue;
		}
	break;
	}

	if(p==NULL){printf("failed to connect\n");return 1;}
	freeaddrinfo(servinfo);


	/// Sending data and/or receiving information

    if(data_send){
    	printf("loading %s\n",filename);
    	r=load_nxy_file(filename,&greg);
    	if(r==NULL){printf("wrong config file \n");exit(1);}
        save = nxydata2str(greg,r,&length);
        if(length<0){printf("length1 = %d\n");return 1;}
    }

    buf = pack(com,&length,save);
    printf("sending %d bytes \n",length);
	if(length<0){printf("length2 = %d\n");return 1;}
    if(save!=NULL)free(save);

	status = sendall(socket_fd,buf,&length);
	printf("sended %d bytes \n",length);
	//status = send(socket_fd,buf,length,0);
	if(data_send){
		status = recv(socket_fd,message,3,0);
		printf("received %d\n",status);
		if(memcmp(message,(char *)"AOK",3)==0)printf("all went well\n");
		else if(memcmp(message,(char *)"BAD",3)==0) printf("something went wrong \n");
		else printf("error \n");
		putc(message[0],stdout);
		putc(message[1],stdout);
		putc(message[2],stdout);
		
	}
	
		
	free(buf);
	if(status<1){printf("send error (%d)\n",status);exit(1);}

	free(r);



	if(data_receive)
    {
        buf = unpackdata(socket_fd,&length,&com);
        if(buf==NULL)return 0;
        printf("receiving %d bytes \n",length);
        r = str2nxydata(buf,&greg);
        if(r==NULL)return 0;
        print_global(greg);
        for(i=0;i<greg.nxy_number;i++){
        	print_nxy_reg(r[i]);
		}
		
		if(data_save){
			save_nxy_file(filename,greg,r);
			}
		free(r);
    }

	//status = recv(socket_fd,message,sizeof(message),0);
	//if(status<1){printf("recv error\n");exit(1);}
	//message[status]='\0';
	//printf("received : %s\n",message);

	close(socket_fd);

	return 0;
	}

